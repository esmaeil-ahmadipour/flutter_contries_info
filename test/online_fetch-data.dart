import 'package:dio/dio.dart';
import 'package:flutter_countries_info/core/utils/resources/mainStrings.dart';
import 'package:flutter_countries_info/data/datasources/remote/countriesInfoApiService.dart';
import 'package:flutter_countries_info/data/repositories/countriesInfoRepositoryImpl.dart';
import 'package:flutter_test/flutter_test.dart';

void main() async {
  group('fetch data on Path: (/all) ', () {
    test('get all countries Info , is success', () async {
      // act
      bool isIbanValidResult = await CountriesInfoRepositoryImpl(
          CountriesInfoApiService(Dio(),
              baseUrl: MainStrings.countriesInfoBaseUrl))
          .getCountriesInfo()
          .then((value) {
        if (value.remoteData != null && value.remoteData!.isNotEmpty) {
          return true;
        } else {
          return false;
        }
      });
      // assert
      expect(isIbanValidResult, true);
    });
  });

}
