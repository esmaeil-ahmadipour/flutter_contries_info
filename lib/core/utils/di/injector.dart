import 'package:dio/dio.dart';
import 'package:flutter_countries_info/data/datasources/remote/countriesInfoApiService.dart';
import 'package:flutter_countries_info/data/repositories/countriesInfoRepositoryImpl.dart';
import 'package:flutter_countries_info/domain/repositories/countriesInfoRepository.dart';
import 'package:flutter_countries_info/domain/usecases/getCountriesInfoUseCase.dart';
import 'package:get_it/get_it.dart';


final injector = GetIt.instance;

Future<void> initializeDependencies() async {

  // Dio client
  injector.registerSingleton<Dio>(Dio());

  // Services
  injector.registerSingleton(CountriesInfoApiService(injector()));

  // Repositories
  injector.registerLazySingleton<CountriesInfoRepository>(()=>CountriesInfoRepositoryImpl(injector()));


  // UseCases
  injector.registerLazySingleton(() => GetCountriesInfoUseCase(injector()));

}

