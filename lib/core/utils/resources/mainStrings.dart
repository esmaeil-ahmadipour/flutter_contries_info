abstract class MainStrings {

  static const countriesInfoBaseUrl = "https://restcountries.com/v3.1";
  static const countriesInfoAll = "/all";
  static const countryCapitalImage ='assets/capital.png';


  // App Title Name
  static const String appTitleName = 'Flutter Countries Info';

  // Routes
  static const root_route = "/";
  static const unknown_route = "/unknown";

  // Localizations
  static const Map<String, String> _english = {
    farsi: "Farsi",
    english: "English",
    darkTheme: "Dark Theme",
    lightTheme: "Light Theme",
    changeLanguage: "Change App Language",
    changeTheme: "Change App Theme",
    headTitle: "Hello, world.",
    unknownRoute: "Page Not Found.",
    exitFromApp: "Exit From App",
    settings: "Settings",
    emptyValidation: "This field should not be empty.",
    incorrectValidation: "The data in this field is incorrect.",
    submitButton: "Submit",
    mainScreen: "Main Screen",
    countryName: "Country Name",
    countryDetails: "Country Details",
    countryCapitals: "Capitals",
    appTitle: "Flutter Countries Info"
  };
  static const Map<String, String> _farsi = {
    farsi: "فارسی",
    english: "انگلیسی",
    darkTheme: "تم شب",
    lightTheme: "تم روز",
    changeLanguage: "تغییر زبان برنامه",
    changeTheme: "تغییر تم برنامه",
    headTitle: "سلام دنیا.",
    unknownRoute: "صفحه پیدا نشد",
    exitFromApp: "خروج از برنامه",
    settings: "تنظیمات",
    emptyValidation: "این قسمت نباید خالی باشد.",
    incorrectValidation: "اطلاعات این بخش نادرست است.",
    submitButton: "ثبت",
    mainScreen: "صفحه اصلی",
    countryName: "نام کشور",
    countryDetails: "جزئیات کشور",
    countryCapitals: "پایتخت ها",
    appTitle: "اطلاعات کشورها در فلاتر"
  };

  static Map<String, String> get en => _english;

  static Map<String, String> get fa => _farsi;

  static const String farsi = '';
  static const String english = 'english';
  static const String darkTheme = 'darkTheme';
  static const String lightTheme = 'lightTheme';
  static const String changeLanguage = 'changeLanguage';
  static const String changeTheme = 'changeTheme';
  static const String headTitle = 'headTitle';
  static const String unknownRoute = 'unknownRoute';
  static const String exitFromApp = 'exitFromApp';
  static const String settings = 'settings';
  static const String emptyValidation = 'emptyValidation';
  static const String incorrectValidation = 'incorrectValidation';
  static const String submitButton = 'submitButton';
  static const String mainScreen = 'mainScreen';
  static const String appTitle = 'appTitle';
  static const String countryName = 'countryName';
  static const String countryDetails = 'countryDetails';
  static const String countryCapitals = 'countryCapitals';
}
