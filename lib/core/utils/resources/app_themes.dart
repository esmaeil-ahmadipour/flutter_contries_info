import 'package:flutter/material.dart';

ValueNotifier<ThemeData> selectedTheme =
    ValueNotifier<ThemeData>(appThemeData[AppTheme.light]!);

enum AppTheme { dark, light }

final appThemeData = {
  AppTheme.light:
      ThemeData(brightness: Brightness.light, primaryColor: Colors.blue),
  AppTheme.dark:
      ThemeData(brightness: Brightness.dark, primaryColor: Colors.blue[700]),
};
