import 'package:flutter/material.dart';
import 'package:flutter_countries_info/core/utils/resources/mainStrings.dart';
import 'package:flutter_countries_info/presentation/views/mainScreen/mainScreen.dart';
import 'package:flutter_countries_info/presentation/views/unknownRoute/unknownRoute.dart';

abstract class AppRoutes {
  static Route? onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case MainStrings.root_route:
        return MaterialPageRoute(builder: (_) => const MainScreen());

      default:
        return MaterialPageRoute(builder: (_) => const UnknownRoute());
    }
  }

  static Route onUnknownRoute(RouteSettings settings) {
    return MaterialPageRoute(builder: (_) => const UnknownRoute());
  }
}
