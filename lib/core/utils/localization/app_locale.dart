import 'package:flutter/material.dart';

ValueNotifier<Locale> selectedLocale =
    ValueNotifier<Locale>(appLocaleData[AppLocalization.en]!);

enum AppLocalization { en, fa }

final appLocaleData = {
  AppLocalization.en: Locale(AppLocalization.en.toString()),
  AppLocalization.fa: Locale(AppLocalization.fa.toString())
};
