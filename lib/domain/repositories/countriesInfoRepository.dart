import 'package:flutter_countries_info/core/params/countriesInfoParams.dart';
import 'package:flutter_countries_info/data/data_sources/remote/remoteDataState.dart';
import 'package:flutter_countries_info/data/models/country/country.dart';

abstract class CountriesInfoRepository {
  Future<RemoteDataState<List<Country>>> getCountriesInfo();
}