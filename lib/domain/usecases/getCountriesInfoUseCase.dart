import 'package:flutter_countries_info/core/usecase/usecase.dart';
import 'package:flutter_countries_info/data/data_sources/remote/remoteDataState.dart';
import 'package:flutter_countries_info/data/models/country/country.dart';
import 'package:flutter_countries_info/domain/repositories/countriesInfoRepository.dart';

class GetCountriesInfoUseCase implements UseCase<RemoteDataState<List<Country>>> {
  final CountriesInfoRepository _countriesInfoRepository;

  GetCountriesInfoUseCase(this._countriesInfoRepository);

  @override
  Future<RemoteDataState<List<Country>>> call() {
    return _countriesInfoRepository.getCountriesInfo();
  }
}
