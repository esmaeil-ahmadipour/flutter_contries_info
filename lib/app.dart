import 'package:flutter/material.dart';
import 'package:flutter_countries_info/core/utils/app_routes.dart';
import 'package:flutter_countries_info/core/utils/localization/appLocalizationsSetup.dart';
import 'package:flutter_countries_info/core/utils/localization/app_locale.dart';
import 'package:flutter_countries_info/core/utils/resources/app_themes.dart';
import 'package:flutter_countries_info/core/utils/resources/mainStrings.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<Locale>(
        valueListenable: selectedLocale,
        builder: (context, selectedLocaleData, child) {
          return ValueListenableBuilder<ThemeData>(
              valueListenable: selectedTheme,
              builder: (context, selectedThemeData, child) {
                return MaterialApp(
                    debugShowCheckedModeBanner: false,
                    theme: selectedThemeData,
                    title: MainStrings.appTitleName,
                    onGenerateRoute: AppRoutes.onGenerateRoute,
                    onUnknownRoute: AppRoutes.onUnknownRoute,
                    supportedLocales: AppLocalizationsSetup.supportedLocales,
                    localizationsDelegates:
                        AppLocalizationsSetup.localizationsDelegates,
                    localeResolutionCallback:
                        AppLocalizationsSetup.localeResolutionCallback,
                    locale: selectedLocaleData);
              });
        });
  }
}
