import 'package:flutter/material.dart';
import 'package:flutter_countries_info/app.dart';
import 'package:flutter_countries_info/core/utils/di/injector.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initializeDependencies();
  runApp(const ProviderScope(child: MyApp()));
}
