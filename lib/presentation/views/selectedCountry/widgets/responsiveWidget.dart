import 'package:flutter/material.dart';

class ResponsiveWidget extends StatelessWidget {
  const ResponsiveWidget(
      {Key? key, required this.mobile, this.tablet, this.web})
      : super(key: key);

  final Widget mobile;
  final Widget? tablet;
  final Widget? web;

  @override
  Widget build(BuildContext context) {
    if (MediaQuery.of(context).size.width < 460) {
      return mobile;
    } else if (MediaQuery.of(context).size.width >= 1300) {
      return web ?? tablet ?? mobile;
    } else {
      return tablet ?? web?? mobile;
    }
  }
}
