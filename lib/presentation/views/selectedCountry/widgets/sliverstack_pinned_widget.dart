import 'package:flutter/material.dart';
import 'package:sliver_tools/sliver_tools.dart';

class SliverStackWithPinnedWidget extends StatelessWidget {
  final Widget  sliverPinned;
  final Widget sliverItem;

    const SliverStackWithPinnedWidget(
      {Key? key,  required this.sliverItem,  required this.sliverPinned}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverStack(
      children: [
        SliverPositioned.fill(
            child: MultiSliver(
          children: [
            if (sliverPinned != null) SliverPinnedHeader(child: sliverPinned),
            sliverItem
          ],
        )),
      ],
    );
  }
}
