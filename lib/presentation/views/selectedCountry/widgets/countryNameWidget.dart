import 'package:delayed_display/delayed_display.dart';
import 'package:flutter/material.dart';
import 'package:flutter_countries_info/core/utils/localization/app_locale.dart';
import 'package:flutter_countries_info/data/models/country/country.dart';

class CountryNameWidget extends StatelessWidget {
  const CountryNameWidget({
    Key? key,
    required this.country,
  }) : super(key: key);

  final Country country;

  @override
  Widget build(BuildContext context) {
    return DelayedDisplay(
      slidingCurve: Curves.easeInExpo,
      delay: const Duration(milliseconds: (500 * 1)),
      child: Text(
        "${(selectedLocale.value.languageCode != "fa" ? country.name!.official : country.translations!.per == null ? "" : country.translations!.per!.official)}",
        textAlign: TextAlign.center,
        style: Theme.of(context).primaryTextTheme.headline5!.copyWith(
            color: Theme.of(context).colorScheme.onSurface,
            fontWeight: FontWeight.bold),
      ),
    );
  }
}
