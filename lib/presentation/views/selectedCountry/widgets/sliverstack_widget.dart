import 'package:flutter/material.dart';
import 'package:sliver_tools/sliver_tools.dart';

class SliverStackWidget extends StatelessWidget {
  final List<Widget> sliverItems;

  const SliverStackWidget({Key? key, required this.sliverItems}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverStack(
      children: [
        SliverPositioned.fill(
            child: MultiSliver(
          children: sliverItems,
        )),
      ],
    );
  }
}
