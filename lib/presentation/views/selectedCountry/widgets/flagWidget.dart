import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_countries_info/data/models/country/country.dart';
import 'package:flutter_countries_info/presentation/views/selectedCountry/widgets/responsiveWidget.dart';

class FlagWidget extends StatelessWidget {
  const FlagWidget({Key? key, required this.country, required this.index})
      : super(key: key);
  final Country country;
  final int index;

  @override
  Widget build(BuildContext context) {

    return ResponsiveWidget(
      mobile: Padding(
        padding: const EdgeInsetsDirectional.only(top: 32.0, bottom: 16.0),
        child: SizedBox(
          height: 150,
          child: HeroWidget(
            index: index,
            country: country,
          ),
        ),
      ),
      tablet: Padding(
        padding: const EdgeInsetsDirectional.only(top: 32.0, bottom: 16.0,start: 16.0),
        child: Align(alignment: AlignmentDirectional.topStart,
          child: SizedBox(
            height: 150,
            child: HeroWidget(
              index: index,
              country: country,
            ),
          ),
        ),
      ),
    );
  }
}

class HeroWidget extends StatelessWidget {
  final Country country;
  final int index;

  const HeroWidget({Key? key, required this.country, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: "HERO_IMAGE_$index",
      child: FittedBox(
          fit: BoxFit.fill,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(25.0)),
              border: Border.all(
                width: 4.0,
                color: Theme.of(context).primaryColor,
              ),
            ),
            child: ClipRRect(
              borderRadius: const BorderRadius.all(Radius.circular(21.0)),
              child: CachedNetworkImage(
                imageUrl: country.flags!.png.toString(),
              ),
            ),
          )),
    );
  }
}
