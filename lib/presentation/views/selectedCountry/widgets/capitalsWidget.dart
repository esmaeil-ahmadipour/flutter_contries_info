import 'package:delayed_display/delayed_display.dart';
import 'package:flutter/material.dart';
import 'package:flutter_countries_info/core/utils/localization/appLocalizations.dart';
import 'package:flutter_countries_info/core/utils/resources/mainStrings.dart';
import 'package:flutter_countries_info/data/models/country/country.dart';
import 'package:flutter_faded_list/flutter_faded_list.dart';
import 'package:flutter_glass/flutter_glass.dart';

class CapitalsWidget extends StatelessWidget {
  const CapitalsWidget({
    Key? key,
    required this.country,
  }) : super(key: key);

  final Country country;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsetsDirectional.only(top: 32.0, bottom: 16.0),
      child: SizedBox(
        height: 150,
        child: Padding(
          padding: const EdgeInsets.only(left: 16.0, right: 16.0),
          child: DelayedDisplay(
            slidingCurve: Curves.easeInOutQuint,
            delay: const Duration(milliseconds: (500 * 2)),
            child: FadedHorizontalList(
              headerColor: Colors.transparent,
              headerWidget: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Text(
                      AppLocalizations.of(context)!
                          .translate(MainStrings.countryCapitals)
                          .toString(),
                      style: Theme.of(context)
                          .primaryTextTheme
                          .headline5!
                          .copyWith(
                              color: Theme.of(context).colorScheme.onPrimary,
                              fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
              bodyColor: Theme.of(context).primaryColor,
              minOpacityOnImage: 0.2,
              imageWidget: Image.asset(
                MainStrings.countryCapitalImage,
                fit: BoxFit.fitHeight,
              ),
              blankSpaceWidth: 0.0,
              isDisabledGlow: true,
              children: [
                for (var i = 0; i < country.capital!.length; ++i)
                  GlassContainer(
                    children: [
                      Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Text(
                            country.capital![i],
                            style: Theme.of(context)
                                .primaryTextTheme
                                .headline5!
                                .copyWith(
                                    color:
                                        Theme.of(context).colorScheme.onPrimary,
                                    fontWeight: FontWeight.bold),
                          )),
                    ],
                    innerPadding: const EdgeInsets.all(0.0),
                    spreadRadius: 2.0,
                    borderWidth: 2.0,
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
