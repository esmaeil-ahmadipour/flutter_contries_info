import 'package:flutter/material.dart';
import 'package:flutter_countries_info/core/utils/localization/appLocalizations.dart';
import 'package:flutter_countries_info/core/utils/resources/mainStrings.dart';
import 'package:flutter_countries_info/core/utils/responsive.dart';
import 'package:flutter_countries_info/data/models/country/country.dart';
import 'package:flutter_countries_info/presentation/views/selectedCountry/widgets/capitalsWidget.dart';
import 'package:flutter_countries_info/presentation/views/selectedCountry/widgets/countryNameWidget.dart';
import 'package:flutter_countries_info/presentation/views/selectedCountry/widgets/flagWidget.dart';
import 'package:flutter_countries_info/presentation/views/selectedCountry/widgets/sliverstack_pinned_widget.dart';

class SelectedCountryScreen extends StatelessWidget {
  const SelectedCountryScreen(
      {Key? key, required this.country, required this.index})
      : super(key: key);
  final Country country;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text(
          AppLocalizations.of(context)!
              .translate(MainStrings.countryDetails)
              .toString(),
          style: TextStyle(color: Theme.of(context).colorScheme.onPrimary),
        )),
        body: CustomScrollView(slivers: [
          SliverStackWithPinnedWidget(
            sliverPinned: Container(
              height: 0.0,
              width: MediaQuery.of(context).size.width,
              color: Theme.of(context).colorScheme.surface,
            ),
            sliverItem:isMobile(context)? Column(children: [
              FlagWidget(country: country, index: index),
              CountryNameWidget(country: country),
              const SizedBox(
                height: 16.0,
              ),
              CapitalsWidget(country: country),
            ]):Column(
              children: [
                CountryNameWidget(country: country),

                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround  ,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                  FlagWidget(country: country, index: index),
                      Flexible(child: CapitalsWidget(country: country)),

                ]),
              ],
            ),
          ),
        ]));
  }
}
