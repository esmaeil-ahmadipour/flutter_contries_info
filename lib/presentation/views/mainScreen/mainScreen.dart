import 'package:flutter/material.dart';
import 'package:flutter_countries_info/core/utils/localization/appLocalizations.dart';
import 'package:flutter_countries_info/core/utils/resources/mainStrings.dart';
import 'package:flutter_countries_info/presentation/views/drawer/custom_drawer.dart';
import 'package:flutter_countries_info/presentation/views/mainScreen/widgets/mainScreenBody.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: const CustomDrawer(),
        appBar: AppBar(
            title: Text(
          AppLocalizations.of(context)!
              .translate(MainStrings.mainScreen)
              .toString(),
          style: TextStyle(color: Theme.of(context).colorScheme.onPrimary),
        )),
        body: const MainScreenBody());
  }
}
