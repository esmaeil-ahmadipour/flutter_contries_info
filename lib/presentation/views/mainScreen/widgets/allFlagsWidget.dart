import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_countries_info/core/utils/localization/app_locale.dart';
import 'package:flutter_countries_info/data/models/country/country.dart';
import 'package:flutter_countries_info/presentation/views/selectedCountry/selectedCountryScreen.dart';

class AllFlagsWidget extends StatelessWidget {
  const AllFlagsWidget({Key? key, required this.list}) : super(key: key);
  final List<Country> list;

  @override
  Widget build(BuildContext context) {
    return MediaQuery.removePadding(
      context: context,
      removeTop: true,
      child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            mainAxisExtent: 200,
            crossAxisCount: MediaQuery.of(context).size.width ~/ 150.0,
          ),
          itemCount: list.length,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) =>  SelectedCountryScreen(country:list[index] ,index:index,)),
                );
              },
              child: Container(
                margin: const EdgeInsets.all(2.0),
                child: Column(
                  children: [
                    Hero(
                      tag: "HERO_IMAGE_${index}",
                      child: FittedBox(
                        fit: BoxFit.fill,
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: 4.0,
                              color: Theme.of(context).primaryColor,
                            ),
                            shape: BoxShape.circle,
                          ),
                          child: CircleAvatar(
                            backgroundColor: Theme.of(context).primaryColor,
                            radius: 48,
                            backgroundImage: CachedNetworkImageProvider(
                              list[index].flags!.png.toString(),
                              scale: 0.5,
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 8.0,
                    ),
                    Text(
                      "${(selectedLocale.value.languageCode != "fa" ? list[index].name!.official : list[index].translations!.per == null ? "" : list[index].translations!.per!.official)}",
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
            );
          }),
    );
  }
}
