import 'package:flutter/material.dart';
import 'package:flutter_countries_info/data/models/country/country.dart';
import 'package:flutter_countries_info/presentation/riverpod/presentCountryList.dart';
import 'package:flutter_countries_info/presentation/views/mainScreen/widgets/allFlagsWidget.dart';
import 'package:flutter_countries_info/presentation/views/mainScreen/widgets/emptyWidget.dart';
import 'package:flutter_countries_info/presentation/views/mainScreen/widgets/errorWidget.dart';
import 'package:flutter_countries_info/presentation/views/mainScreen/widgets/loadingWidget.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class MainScreenBody extends StatelessWidget {
  const MainScreenBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (BuildContext context, WidgetRef ref, Widget? child) {
        final Future<List<Country>> value = ref.watch(countryProvider.future);
        return FutureBuilder<List<Country>>(
          future: value,
          builder: (
            BuildContext context,
            AsyncSnapshot<List<Country>> snapshot,
          ) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const LoadingWidget();
            } else if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.hasError) {
                return const WidgetError();
              } else if (snapshot.hasData) {
                final List<Country> list = snapshot.data as List<Country>;
                return AllFlagsWidget(
                  list: list,
                );
              } else {
                return const EmptyWidget();
              }
            } else {
              return const SizedBox();
            }
          },
        );
      },
    );
  }
}
