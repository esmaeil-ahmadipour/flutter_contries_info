import 'package:flutter/material.dart';
import 'package:flutter_countries_info/core/utils/localization/appLocalizations.dart';
import 'package:flutter_countries_info/core/utils/resources/mainStrings.dart';

class UnknownRoute extends StatelessWidget {
  const UnknownRoute({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: Text(
            AppLocalizations.of(context)!.translate(MainStrings.unknownRoute)!),
      ),
    );
  }
}
