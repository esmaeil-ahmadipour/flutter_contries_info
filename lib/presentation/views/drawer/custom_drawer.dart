import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_countries_info/core/utils/localization/appLocalizations.dart';
import 'package:flutter_countries_info/core/utils/localization/app_locale.dart';
import 'package:flutter_countries_info/core/utils/resources/app_themes.dart';
import 'package:flutter_countries_info/core/utils/resources/mainStrings.dart';
import 'package:flutter_countries_info/presentation/views/drawer/widgets/custom_drawer_item.dart';
import 'package:flutter_countries_info/presentation/views/drawer/widgets/drawer_title_widget.dart';
import 'package:move_to_background/move_to_background.dart';

class CustomDrawer extends StatelessWidget {
  const CustomDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 180,
              width: double.infinity,
              color: Theme.of(context).primaryColor,
              child: Center(
                child: Text(
                  AppLocalizations.of(context)!
                      .translate(MainStrings.appTitle)!,
                  style: TextStyle(
                      color: Theme.of(context).colorScheme.onPrimary,
                      fontSize: Theme.of(context)
                          .primaryTextTheme
                          .headline6!
                          .fontSize),
                ),
              ),
            ),
            const DrawerSettingsTitleWidget(),
            ValueListenableBuilder<ThemeData>(
              valueListenable: selectedTheme,
              builder: (context, selectedThemeData, child) {
                return DrawerItemWidget(
                  iconData: Theme.of(context).brightness.index == 0
                      ? Icons.wb_sunny
                      : Icons.nightlight_outlined,
                  itemText: AppLocalizations.of(context)!
                      .translate(MainStrings.changeTheme)!,
                  onTap: () {
                    if (selectedTheme.value.brightness == Brightness.dark) {
                      selectedTheme.value =
                          ThemeData(brightness: Brightness.light);
                    } else {
                      selectedTheme.value =
                          ThemeData(brightness: Brightness.dark);
                    }
                  },
                );
              },
            ),
            ValueListenableBuilder<Locale>(
              valueListenable: selectedLocale,
              builder: (context, selectedThemeData, child) {
                return DrawerItemWidget(
                  iconData: AppLocalizations.of(context)!.isEnLocale == false
                      ? Icons.format_align_right_outlined
                      : Icons.format_align_left_outlined,
                  itemText: AppLocalizations.of(context)!
                      .translate(MainStrings.changeLanguage)!,
                  onTap: () {
                    if (selectedLocale.value.languageCode ==
                        AppLocalization.en.name) {
                      selectedLocale.value = Locale(AppLocalization.fa.name);
                    } else {
                      selectedLocale.value = Locale(AppLocalization.en.name);
                    }
                  },
                );
              },
            ),
            DrawerItemWidget(
                iconData: Icons.exit_to_app,
                itemText: AppLocalizations.of(context)!
                    .translate(MainStrings.exitFromApp)!,
                onTap: () {
                  if (Platform.isAndroid) {
                    MoveToBackground.moveTaskToBack();
                  } else if (Platform.isIOS) {
                    exit(0);
                  }
                })
          ],
        ),
      ),
    );
  }
}
