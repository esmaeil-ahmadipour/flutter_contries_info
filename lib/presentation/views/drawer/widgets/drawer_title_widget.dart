import 'package:flutter/material.dart';
import 'package:flutter_countries_info/core/utils/localization/appLocalizations.dart';
import 'package:flutter_countries_info/core/utils/resources/mainStrings.dart';

class DrawerSettingsTitleWidget extends StatelessWidget {
  const DrawerSettingsTitleWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 64,
      child: Container(
        padding: const EdgeInsetsDirectional.only(start: 16),
        alignment: AlignmentDirectional.centerStart,
        child: Text(
          AppLocalizations.of(context)!.translate(MainStrings.settings)!,
          style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
