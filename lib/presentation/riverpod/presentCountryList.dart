import 'package:dio/dio.dart';
import 'package:flutter_countries_info/core/utils/resources/mainStrings.dart';
import 'package:flutter_countries_info/data/datasources/remote/countriesInfoApiService.dart';
import 'package:flutter_countries_info/data/models/country/country.dart';
import 'package:flutter_countries_info/data/repositories/countriesInfoRepositoryImpl.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final countryProvider =
    StateNotifierProvider.autoDispose<CountryList, AsyncValue<List<Country>>>(
        (ref) {
  return CountryList();
});

class CountryList extends StateNotifier<AsyncValue<List<Country>>> {
  CountryList() : super(const AsyncValue.loading()) {
    _fetch();
  }

  Future<void> _fetch() async {
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(() => getData());
  }

  Future<List<Country>> getData() async {
    List<Country> countries = [];
    await CountriesInfoRepositoryImpl(CountriesInfoApiService(Dio(),
            baseUrl: MainStrings.countriesInfoBaseUrl))
        .getCountriesInfo()
        .then((value) {
      countries = value.remoteData!;
    });
    return countries;
  }
}
