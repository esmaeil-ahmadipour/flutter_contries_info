import 'package:dio/dio.dart';
import 'package:flutter_countries_info/core/utils/resources/mainStrings.dart';
import 'package:flutter_countries_info/data/models/country/country.dart';
import 'package:retrofit/retrofit.dart';

part 'countriesInfoApiService.g.dart';

@RestApi(baseUrl: MainStrings.countriesInfoBaseUrl)
abstract class CountriesInfoApiService {
  factory CountriesInfoApiService(Dio dio, {String baseUrl}) = _CountriesInfoApiService;

  @GET('/all')
  Future<HttpResponse<List<Country>>> getAllCountries(
  );
}
