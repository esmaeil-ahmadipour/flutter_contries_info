// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'demonyms.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Demonyms _$DemonymsFromJson(Map<String, dynamic> json) {
  return _Demonyms.fromJson(json);
}

/// @nodoc
class _$DemonymsTearOff {
  const _$DemonymsTearOff();

  _Demonyms call({required Eng? eng, required Eng? fra}) {
    return _Demonyms(
      eng: eng,
      fra: fra,
    );
  }

  Demonyms fromJson(Map<String, Object?> json) {
    return Demonyms.fromJson(json);
  }
}

/// @nodoc
const $Demonyms = _$DemonymsTearOff();

/// @nodoc
mixin _$Demonyms {
  Eng? get eng => throw _privateConstructorUsedError;
  Eng? get fra => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DemonymsCopyWith<Demonyms> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DemonymsCopyWith<$Res> {
  factory $DemonymsCopyWith(Demonyms value, $Res Function(Demonyms) then) =
      _$DemonymsCopyWithImpl<$Res>;
  $Res call({Eng? eng, Eng? fra});

  $EngCopyWith<$Res>? get eng;
  $EngCopyWith<$Res>? get fra;
}

/// @nodoc
class _$DemonymsCopyWithImpl<$Res> implements $DemonymsCopyWith<$Res> {
  _$DemonymsCopyWithImpl(this._value, this._then);

  final Demonyms _value;
  // ignore: unused_field
  final $Res Function(Demonyms) _then;

  @override
  $Res call({
    Object? eng = freezed,
    Object? fra = freezed,
  }) {
    return _then(_value.copyWith(
      eng: eng == freezed
          ? _value.eng
          : eng // ignore: cast_nullable_to_non_nullable
              as Eng?,
      fra: fra == freezed
          ? _value.fra
          : fra // ignore: cast_nullable_to_non_nullable
              as Eng?,
    ));
  }

  @override
  $EngCopyWith<$Res>? get eng {
    if (_value.eng == null) {
      return null;
    }

    return $EngCopyWith<$Res>(_value.eng!, (value) {
      return _then(_value.copyWith(eng: value));
    });
  }

  @override
  $EngCopyWith<$Res>? get fra {
    if (_value.fra == null) {
      return null;
    }

    return $EngCopyWith<$Res>(_value.fra!, (value) {
      return _then(_value.copyWith(fra: value));
    });
  }
}

/// @nodoc
abstract class _$DemonymsCopyWith<$Res> implements $DemonymsCopyWith<$Res> {
  factory _$DemonymsCopyWith(_Demonyms value, $Res Function(_Demonyms) then) =
      __$DemonymsCopyWithImpl<$Res>;
  @override
  $Res call({Eng? eng, Eng? fra});

  @override
  $EngCopyWith<$Res>? get eng;
  @override
  $EngCopyWith<$Res>? get fra;
}

/// @nodoc
class __$DemonymsCopyWithImpl<$Res> extends _$DemonymsCopyWithImpl<$Res>
    implements _$DemonymsCopyWith<$Res> {
  __$DemonymsCopyWithImpl(_Demonyms _value, $Res Function(_Demonyms) _then)
      : super(_value, (v) => _then(v as _Demonyms));

  @override
  _Demonyms get _value => super._value as _Demonyms;

  @override
  $Res call({
    Object? eng = freezed,
    Object? fra = freezed,
  }) {
    return _then(_Demonyms(
      eng: eng == freezed
          ? _value.eng
          : eng // ignore: cast_nullable_to_non_nullable
              as Eng?,
      fra: fra == freezed
          ? _value.fra
          : fra // ignore: cast_nullable_to_non_nullable
              as Eng?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Demonyms extends _Demonyms {
  const _$_Demonyms({required this.eng, required this.fra}) : super._();

  factory _$_Demonyms.fromJson(Map<String, dynamic> json) =>
      _$$_DemonymsFromJson(json);

  @override
  final Eng? eng;
  @override
  final Eng? fra;

  @override
  String toString() {
    return 'Demonyms(eng: $eng, fra: $fra)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Demonyms &&
            const DeepCollectionEquality().equals(other.eng, eng) &&
            const DeepCollectionEquality().equals(other.fra, fra));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(eng),
      const DeepCollectionEquality().hash(fra));

  @JsonKey(ignore: true)
  @override
  _$DemonymsCopyWith<_Demonyms> get copyWith =>
      __$DemonymsCopyWithImpl<_Demonyms>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_DemonymsToJson(this);
  }
}

abstract class _Demonyms extends Demonyms {
  const factory _Demonyms({required Eng? eng, required Eng? fra}) = _$_Demonyms;
  const _Demonyms._() : super._();

  factory _Demonyms.fromJson(Map<String, dynamic> json) = _$_Demonyms.fromJson;

  @override
  Eng? get eng;
  @override
  Eng? get fra;
  @override
  @JsonKey(ignore: true)
  _$DemonymsCopyWith<_Demonyms> get copyWith =>
      throw _privateConstructorUsedError;
}
