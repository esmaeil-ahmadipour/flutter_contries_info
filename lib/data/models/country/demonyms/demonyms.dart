import 'package:flutter_countries_info/data/models/country/eng/eng.dart';
import 'package:freezed_annotation/freezed_annotation.dart' ;
part 'demonyms.g.dart';
part 'demonyms.freezed.dart';

@freezed
class Demonyms with _$Demonyms {
  const Demonyms._();
  const factory Demonyms({
    required Eng? eng,
    required Eng? fra,

  }) = _Demonyms;
  factory Demonyms.fromJson(Map<String, dynamic> json) => _$DemonymsFromJson(json);
}
