import 'package:freezed_annotation/freezed_annotation.dart' ;
part 'idd.g.dart';
part 'idd.freezed.dart';

@freezed
class Idd with _$Idd {
  const Idd._();
  const factory Idd({
    required  String? root,
    required  List<String>? suffixes,

  }) = _Idd;
  factory Idd.fromJson(Map<String, dynamic> json) => _$IddFromJson(json);
}
