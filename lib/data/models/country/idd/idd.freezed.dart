// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'idd.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Idd _$IddFromJson(Map<String, dynamic> json) {
  return _Idd.fromJson(json);
}

/// @nodoc
class _$IddTearOff {
  const _$IddTearOff();

  _Idd call({required String? root, required List<String>? suffixes}) {
    return _Idd(
      root: root,
      suffixes: suffixes,
    );
  }

  Idd fromJson(Map<String, Object?> json) {
    return Idd.fromJson(json);
  }
}

/// @nodoc
const $Idd = _$IddTearOff();

/// @nodoc
mixin _$Idd {
  String? get root => throw _privateConstructorUsedError;
  List<String>? get suffixes => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $IddCopyWith<Idd> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $IddCopyWith<$Res> {
  factory $IddCopyWith(Idd value, $Res Function(Idd) then) =
      _$IddCopyWithImpl<$Res>;
  $Res call({String? root, List<String>? suffixes});
}

/// @nodoc
class _$IddCopyWithImpl<$Res> implements $IddCopyWith<$Res> {
  _$IddCopyWithImpl(this._value, this._then);

  final Idd _value;
  // ignore: unused_field
  final $Res Function(Idd) _then;

  @override
  $Res call({
    Object? root = freezed,
    Object? suffixes = freezed,
  }) {
    return _then(_value.copyWith(
      root: root == freezed
          ? _value.root
          : root // ignore: cast_nullable_to_non_nullable
              as String?,
      suffixes: suffixes == freezed
          ? _value.suffixes
          : suffixes // ignore: cast_nullable_to_non_nullable
              as List<String>?,
    ));
  }
}

/// @nodoc
abstract class _$IddCopyWith<$Res> implements $IddCopyWith<$Res> {
  factory _$IddCopyWith(_Idd value, $Res Function(_Idd) then) =
      __$IddCopyWithImpl<$Res>;
  @override
  $Res call({String? root, List<String>? suffixes});
}

/// @nodoc
class __$IddCopyWithImpl<$Res> extends _$IddCopyWithImpl<$Res>
    implements _$IddCopyWith<$Res> {
  __$IddCopyWithImpl(_Idd _value, $Res Function(_Idd) _then)
      : super(_value, (v) => _then(v as _Idd));

  @override
  _Idd get _value => super._value as _Idd;

  @override
  $Res call({
    Object? root = freezed,
    Object? suffixes = freezed,
  }) {
    return _then(_Idd(
      root: root == freezed
          ? _value.root
          : root // ignore: cast_nullable_to_non_nullable
              as String?,
      suffixes: suffixes == freezed
          ? _value.suffixes
          : suffixes // ignore: cast_nullable_to_non_nullable
              as List<String>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Idd extends _Idd {
  const _$_Idd({required this.root, required this.suffixes}) : super._();

  factory _$_Idd.fromJson(Map<String, dynamic> json) => _$$_IddFromJson(json);

  @override
  final String? root;
  @override
  final List<String>? suffixes;

  @override
  String toString() {
    return 'Idd(root: $root, suffixes: $suffixes)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Idd &&
            const DeepCollectionEquality().equals(other.root, root) &&
            const DeepCollectionEquality().equals(other.suffixes, suffixes));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(root),
      const DeepCollectionEquality().hash(suffixes));

  @JsonKey(ignore: true)
  @override
  _$IddCopyWith<_Idd> get copyWith =>
      __$IddCopyWithImpl<_Idd>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_IddToJson(this);
  }
}

abstract class _Idd extends Idd {
  const factory _Idd({required String? root, required List<String>? suffixes}) =
      _$_Idd;
  const _Idd._() : super._();

  factory _Idd.fromJson(Map<String, dynamic> json) = _$_Idd.fromJson;

  @override
  String? get root;
  @override
  List<String>? get suffixes;
  @override
  @JsonKey(ignore: true)
  _$IddCopyWith<_Idd> get copyWith => throw _privateConstructorUsedError;
}
