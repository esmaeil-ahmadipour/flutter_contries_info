// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'dkk.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

DKK _$DKKFromJson(Map<String, dynamic> json) {
  return _DKK.fromJson(json);
}

/// @nodoc
class _$DKKTearOff {
  const _$DKKTearOff();

  _DKK call({required String? name, required String? symbol}) {
    return _DKK(
      name: name,
      symbol: symbol,
    );
  }

  DKK fromJson(Map<String, Object?> json) {
    return DKK.fromJson(json);
  }
}

/// @nodoc
const $DKK = _$DKKTearOff();

/// @nodoc
mixin _$DKK {
  String? get name => throw _privateConstructorUsedError;
  String? get symbol => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DKKCopyWith<DKK> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DKKCopyWith<$Res> {
  factory $DKKCopyWith(DKK value, $Res Function(DKK) then) =
      _$DKKCopyWithImpl<$Res>;
  $Res call({String? name, String? symbol});
}

/// @nodoc
class _$DKKCopyWithImpl<$Res> implements $DKKCopyWith<$Res> {
  _$DKKCopyWithImpl(this._value, this._then);

  final DKK _value;
  // ignore: unused_field
  final $Res Function(DKK) _then;

  @override
  $Res call({
    Object? name = freezed,
    Object? symbol = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      symbol: symbol == freezed
          ? _value.symbol
          : symbol // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$DKKCopyWith<$Res> implements $DKKCopyWith<$Res> {
  factory _$DKKCopyWith(_DKK value, $Res Function(_DKK) then) =
      __$DKKCopyWithImpl<$Res>;
  @override
  $Res call({String? name, String? symbol});
}

/// @nodoc
class __$DKKCopyWithImpl<$Res> extends _$DKKCopyWithImpl<$Res>
    implements _$DKKCopyWith<$Res> {
  __$DKKCopyWithImpl(_DKK _value, $Res Function(_DKK) _then)
      : super(_value, (v) => _then(v as _DKK));

  @override
  _DKK get _value => super._value as _DKK;

  @override
  $Res call({
    Object? name = freezed,
    Object? symbol = freezed,
  }) {
    return _then(_DKK(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      symbol: symbol == freezed
          ? _value.symbol
          : symbol // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_DKK extends _DKK {
  const _$_DKK({required this.name, required this.symbol}) : super._();

  factory _$_DKK.fromJson(Map<String, dynamic> json) => _$$_DKKFromJson(json);

  @override
  final String? name;
  @override
  final String? symbol;

  @override
  String toString() {
    return 'DKK(name: $name, symbol: $symbol)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _DKK &&
            const DeepCollectionEquality().equals(other.name, name) &&
            const DeepCollectionEquality().equals(other.symbol, symbol));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(name),
      const DeepCollectionEquality().hash(symbol));

  @JsonKey(ignore: true)
  @override
  _$DKKCopyWith<_DKK> get copyWith =>
      __$DKKCopyWithImpl<_DKK>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_DKKToJson(this);
  }
}

abstract class _DKK extends DKK {
  const factory _DKK({required String? name, required String? symbol}) = _$_DKK;
  const _DKK._() : super._();

  factory _DKK.fromJson(Map<String, dynamic> json) = _$_DKK.fromJson;

  @override
  String? get name;
  @override
  String? get symbol;
  @override
  @JsonKey(ignore: true)
  _$DKKCopyWith<_DKK> get copyWith => throw _privateConstructorUsedError;
}
