// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dkk.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_DKK _$$_DKKFromJson(Map<String, dynamic> json) => _$_DKK(
      name: json['name'] as String?,
      symbol: json['symbol'] as String?,
    );

Map<String, dynamic> _$$_DKKToJson(_$_DKK instance) => <String, dynamic>{
      'name': instance.name,
      'symbol': instance.symbol,
    };
