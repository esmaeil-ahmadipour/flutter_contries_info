import 'package:freezed_annotation/freezed_annotation.dart' ;
part 'dkk.g.dart';
part 'dkk.freezed.dart';

@freezed
class DKK with _$DKK {
  const DKK._();
  const factory DKK({
    required  String? name,
    required  String? symbol,


  }) = _DKK;
  factory DKK.fromJson(Map<String, dynamic> json) => _$DKKFromJson(json);
}
