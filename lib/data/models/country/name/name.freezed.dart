// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'name.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Name _$NameFromJson(Map<String, dynamic> json) {
  return _Name.fromJson(json);
}

/// @nodoc
class _$NameTearOff {
  const _$NameTearOff();

  _Name call(
      {required String? common,
      required String? official,
      required NativeName? nativeName}) {
    return _Name(
      common: common,
      official: official,
      nativeName: nativeName,
    );
  }

  Name fromJson(Map<String, Object?> json) {
    return Name.fromJson(json);
  }
}

/// @nodoc
const $Name = _$NameTearOff();

/// @nodoc
mixin _$Name {
  String? get common => throw _privateConstructorUsedError;
  String? get official => throw _privateConstructorUsedError;
  NativeName? get nativeName => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $NameCopyWith<Name> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NameCopyWith<$Res> {
  factory $NameCopyWith(Name value, $Res Function(Name) then) =
      _$NameCopyWithImpl<$Res>;
  $Res call({String? common, String? official, NativeName? nativeName});

  $NativeNameCopyWith<$Res>? get nativeName;
}

/// @nodoc
class _$NameCopyWithImpl<$Res> implements $NameCopyWith<$Res> {
  _$NameCopyWithImpl(this._value, this._then);

  final Name _value;
  // ignore: unused_field
  final $Res Function(Name) _then;

  @override
  $Res call({
    Object? common = freezed,
    Object? official = freezed,
    Object? nativeName = freezed,
  }) {
    return _then(_value.copyWith(
      common: common == freezed
          ? _value.common
          : common // ignore: cast_nullable_to_non_nullable
              as String?,
      official: official == freezed
          ? _value.official
          : official // ignore: cast_nullable_to_non_nullable
              as String?,
      nativeName: nativeName == freezed
          ? _value.nativeName
          : nativeName // ignore: cast_nullable_to_non_nullable
              as NativeName?,
    ));
  }

  @override
  $NativeNameCopyWith<$Res>? get nativeName {
    if (_value.nativeName == null) {
      return null;
    }

    return $NativeNameCopyWith<$Res>(_value.nativeName!, (value) {
      return _then(_value.copyWith(nativeName: value));
    });
  }
}

/// @nodoc
abstract class _$NameCopyWith<$Res> implements $NameCopyWith<$Res> {
  factory _$NameCopyWith(_Name value, $Res Function(_Name) then) =
      __$NameCopyWithImpl<$Res>;
  @override
  $Res call({String? common, String? official, NativeName? nativeName});

  @override
  $NativeNameCopyWith<$Res>? get nativeName;
}

/// @nodoc
class __$NameCopyWithImpl<$Res> extends _$NameCopyWithImpl<$Res>
    implements _$NameCopyWith<$Res> {
  __$NameCopyWithImpl(_Name _value, $Res Function(_Name) _then)
      : super(_value, (v) => _then(v as _Name));

  @override
  _Name get _value => super._value as _Name;

  @override
  $Res call({
    Object? common = freezed,
    Object? official = freezed,
    Object? nativeName = freezed,
  }) {
    return _then(_Name(
      common: common == freezed
          ? _value.common
          : common // ignore: cast_nullable_to_non_nullable
              as String?,
      official: official == freezed
          ? _value.official
          : official // ignore: cast_nullable_to_non_nullable
              as String?,
      nativeName: nativeName == freezed
          ? _value.nativeName
          : nativeName // ignore: cast_nullable_to_non_nullable
              as NativeName?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Name extends _Name {
  const _$_Name(
      {required this.common, required this.official, required this.nativeName})
      : super._();

  factory _$_Name.fromJson(Map<String, dynamic> json) => _$$_NameFromJson(json);

  @override
  final String? common;
  @override
  final String? official;
  @override
  final NativeName? nativeName;

  @override
  String toString() {
    return 'Name(common: $common, official: $official, nativeName: $nativeName)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Name &&
            const DeepCollectionEquality().equals(other.common, common) &&
            const DeepCollectionEquality().equals(other.official, official) &&
            const DeepCollectionEquality()
                .equals(other.nativeName, nativeName));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(common),
      const DeepCollectionEquality().hash(official),
      const DeepCollectionEquality().hash(nativeName));

  @JsonKey(ignore: true)
  @override
  _$NameCopyWith<_Name> get copyWith =>
      __$NameCopyWithImpl<_Name>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_NameToJson(this);
  }
}

abstract class _Name extends Name {
  const factory _Name(
      {required String? common,
      required String? official,
      required NativeName? nativeName}) = _$_Name;
  const _Name._() : super._();

  factory _Name.fromJson(Map<String, dynamic> json) = _$_Name.fromJson;

  @override
  String? get common;
  @override
  String? get official;
  @override
  NativeName? get nativeName;
  @override
  @JsonKey(ignore: true)
  _$NameCopyWith<_Name> get copyWith => throw _privateConstructorUsedError;
}
