import 'package:flutter_countries_info/data/models/country/nativeName/nativeName.dart';
import 'package:freezed_annotation/freezed_annotation.dart' ;
part 'name.g.dart';
part 'name.freezed.dart';

@freezed
class Name with _$Name {
  const Name._();
  const factory Name({
    required String? common,
    required String? official,
    required NativeName? nativeName,

  }) = _Name;
  factory Name.fromJson(Map<String, dynamic> json) => _$NameFromJson(json);
}
