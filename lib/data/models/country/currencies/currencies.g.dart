// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'currencies.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Currencies _$$_CurrenciesFromJson(Map<String, dynamic> json) =>
    _$_Currencies(
      dkk: json['dkk'] == null
          ? null
          : DKK.fromJson(json['dkk'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_CurrenciesToJson(_$_Currencies instance) =>
    <String, dynamic>{
      'dkk': instance.dkk,
    };
