// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'currencies.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Currencies _$CurrenciesFromJson(Map<String, dynamic> json) {
  return _Currencies.fromJson(json);
}

/// @nodoc
class _$CurrenciesTearOff {
  const _$CurrenciesTearOff();

  _Currencies call({required DKK? dkk}) {
    return _Currencies(
      dkk: dkk,
    );
  }

  Currencies fromJson(Map<String, Object?> json) {
    return Currencies.fromJson(json);
  }
}

/// @nodoc
const $Currencies = _$CurrenciesTearOff();

/// @nodoc
mixin _$Currencies {
  DKK? get dkk => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CurrenciesCopyWith<Currencies> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CurrenciesCopyWith<$Res> {
  factory $CurrenciesCopyWith(
          Currencies value, $Res Function(Currencies) then) =
      _$CurrenciesCopyWithImpl<$Res>;
  $Res call({DKK? dkk});

  $DKKCopyWith<$Res>? get dkk;
}

/// @nodoc
class _$CurrenciesCopyWithImpl<$Res> implements $CurrenciesCopyWith<$Res> {
  _$CurrenciesCopyWithImpl(this._value, this._then);

  final Currencies _value;
  // ignore: unused_field
  final $Res Function(Currencies) _then;

  @override
  $Res call({
    Object? dkk = freezed,
  }) {
    return _then(_value.copyWith(
      dkk: dkk == freezed
          ? _value.dkk
          : dkk // ignore: cast_nullable_to_non_nullable
              as DKK?,
    ));
  }

  @override
  $DKKCopyWith<$Res>? get dkk {
    if (_value.dkk == null) {
      return null;
    }

    return $DKKCopyWith<$Res>(_value.dkk!, (value) {
      return _then(_value.copyWith(dkk: value));
    });
  }
}

/// @nodoc
abstract class _$CurrenciesCopyWith<$Res> implements $CurrenciesCopyWith<$Res> {
  factory _$CurrenciesCopyWith(
          _Currencies value, $Res Function(_Currencies) then) =
      __$CurrenciesCopyWithImpl<$Res>;
  @override
  $Res call({DKK? dkk});

  @override
  $DKKCopyWith<$Res>? get dkk;
}

/// @nodoc
class __$CurrenciesCopyWithImpl<$Res> extends _$CurrenciesCopyWithImpl<$Res>
    implements _$CurrenciesCopyWith<$Res> {
  __$CurrenciesCopyWithImpl(
      _Currencies _value, $Res Function(_Currencies) _then)
      : super(_value, (v) => _then(v as _Currencies));

  @override
  _Currencies get _value => super._value as _Currencies;

  @override
  $Res call({
    Object? dkk = freezed,
  }) {
    return _then(_Currencies(
      dkk: dkk == freezed
          ? _value.dkk
          : dkk // ignore: cast_nullable_to_non_nullable
              as DKK?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Currencies extends _Currencies {
  const _$_Currencies({required this.dkk}) : super._();

  factory _$_Currencies.fromJson(Map<String, dynamic> json) =>
      _$$_CurrenciesFromJson(json);

  @override
  final DKK? dkk;

  @override
  String toString() {
    return 'Currencies(dkk: $dkk)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Currencies &&
            const DeepCollectionEquality().equals(other.dkk, dkk));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(dkk));

  @JsonKey(ignore: true)
  @override
  _$CurrenciesCopyWith<_Currencies> get copyWith =>
      __$CurrenciesCopyWithImpl<_Currencies>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CurrenciesToJson(this);
  }
}

abstract class _Currencies extends Currencies {
  const factory _Currencies({required DKK? dkk}) = _$_Currencies;
  const _Currencies._() : super._();

  factory _Currencies.fromJson(Map<String, dynamic> json) =
      _$_Currencies.fromJson;

  @override
  DKK? get dkk;
  @override
  @JsonKey(ignore: true)
  _$CurrenciesCopyWith<_Currencies> get copyWith =>
      throw _privateConstructorUsedError;
}
