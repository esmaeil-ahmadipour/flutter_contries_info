import 'package:flutter_countries_info/data/models/country/dkk/dkk.dart';
import 'package:freezed_annotation/freezed_annotation.dart' ;
part 'currencies.g.dart';
part 'currencies.freezed.dart';

@freezed
class Currencies with _$Currencies {
  const Currencies._();
  const factory Currencies({
    required  DKK? dkk,

  }) = _Currencies;
  factory Currencies.fromJson(Map<String, dynamic> json) => _$CurrenciesFromJson(json);
}
