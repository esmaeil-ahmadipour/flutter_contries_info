// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'kal.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Kal _$$_KalFromJson(Map<String, dynamic> json) => _$_Kal(
      official: json['official'] as String?,
      common: json['common'] as String?,
    );

Map<String, dynamic> _$$_KalToJson(_$_Kal instance) => <String, dynamic>{
      'official': instance.official,
      'common': instance.common,
    };
