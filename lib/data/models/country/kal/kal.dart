import 'package:freezed_annotation/freezed_annotation.dart' ;
part 'kal.g.dart';
part 'kal.freezed.dart';

@freezed
class Kal with _$Kal {
  const Kal._();
  const factory Kal({
    required String? official,
    required String? common,
  }) = _Kal;
  factory Kal.fromJson(Map<String, dynamic> json) => _$KalFromJson(json);
}
