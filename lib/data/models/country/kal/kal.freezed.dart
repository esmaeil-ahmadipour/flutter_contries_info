// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'kal.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Kal _$KalFromJson(Map<String, dynamic> json) {
  return _Kal.fromJson(json);
}

/// @nodoc
class _$KalTearOff {
  const _$KalTearOff();

  _Kal call({required String? official, required String? common}) {
    return _Kal(
      official: official,
      common: common,
    );
  }

  Kal fromJson(Map<String, Object?> json) {
    return Kal.fromJson(json);
  }
}

/// @nodoc
const $Kal = _$KalTearOff();

/// @nodoc
mixin _$Kal {
  String? get official => throw _privateConstructorUsedError;
  String? get common => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $KalCopyWith<Kal> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $KalCopyWith<$Res> {
  factory $KalCopyWith(Kal value, $Res Function(Kal) then) =
      _$KalCopyWithImpl<$Res>;
  $Res call({String? official, String? common});
}

/// @nodoc
class _$KalCopyWithImpl<$Res> implements $KalCopyWith<$Res> {
  _$KalCopyWithImpl(this._value, this._then);

  final Kal _value;
  // ignore: unused_field
  final $Res Function(Kal) _then;

  @override
  $Res call({
    Object? official = freezed,
    Object? common = freezed,
  }) {
    return _then(_value.copyWith(
      official: official == freezed
          ? _value.official
          : official // ignore: cast_nullable_to_non_nullable
              as String?,
      common: common == freezed
          ? _value.common
          : common // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$KalCopyWith<$Res> implements $KalCopyWith<$Res> {
  factory _$KalCopyWith(_Kal value, $Res Function(_Kal) then) =
      __$KalCopyWithImpl<$Res>;
  @override
  $Res call({String? official, String? common});
}

/// @nodoc
class __$KalCopyWithImpl<$Res> extends _$KalCopyWithImpl<$Res>
    implements _$KalCopyWith<$Res> {
  __$KalCopyWithImpl(_Kal _value, $Res Function(_Kal) _then)
      : super(_value, (v) => _then(v as _Kal));

  @override
  _Kal get _value => super._value as _Kal;

  @override
  $Res call({
    Object? official = freezed,
    Object? common = freezed,
  }) {
    return _then(_Kal(
      official: official == freezed
          ? _value.official
          : official // ignore: cast_nullable_to_non_nullable
              as String?,
      common: common == freezed
          ? _value.common
          : common // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Kal extends _Kal {
  const _$_Kal({required this.official, required this.common}) : super._();

  factory _$_Kal.fromJson(Map<String, dynamic> json) => _$$_KalFromJson(json);

  @override
  final String? official;
  @override
  final String? common;

  @override
  String toString() {
    return 'Kal(official: $official, common: $common)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Kal &&
            const DeepCollectionEquality().equals(other.official, official) &&
            const DeepCollectionEquality().equals(other.common, common));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(official),
      const DeepCollectionEquality().hash(common));

  @JsonKey(ignore: true)
  @override
  _$KalCopyWith<_Kal> get copyWith =>
      __$KalCopyWithImpl<_Kal>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_KalToJson(this);
  }
}

abstract class _Kal extends Kal {
  const factory _Kal({required String? official, required String? common}) =
      _$_Kal;
  const _Kal._() : super._();

  factory _Kal.fromJson(Map<String, dynamic> json) = _$_Kal.fromJson;

  @override
  String? get official;
  @override
  String? get common;
  @override
  @JsonKey(ignore: true)
  _$KalCopyWith<_Kal> get copyWith => throw _privateConstructorUsedError;
}
