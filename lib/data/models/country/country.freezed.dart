// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'country.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Country _$CountryFromJson(Map<String, dynamic> json) {
  return _Country.fromJson(json);
}

/// @nodoc
class _$CountryTearOff {
  const _$CountryTearOff();

  _Country call(
      {required String? googleCountry,
      required String? openStreetCountry,
      required Name? name,
      required List<String>? tld,
      required String? cca2,
      required String? ccn3,
      required String? cca3,
      required bool? independent,
      required String? status,
      required bool? unMember,
      required Currencies? currencies,
      required Idd? idd,
      required List<String>? capital,
      required List<String>? altSpellings,
      required String? region,
      required String? subregion,
      required Languages? languages,
      required Translations? translations,
      required List<double>? latlng,
      required bool? landlocked,
      required double? area,
      required Demonyms? demonyms,
      required String? flag,
      required Maps? maps,
      required int? population,
      required Map<dynamic, dynamic>? car,
      required List<String>? timezones,
      required List<String>? continents,
      required Flags? flags,
      required Flags? coatOfArms,
      required String? startOfWeek,
      required CapitalInfo? capitalInfo,
      required PostalCode? postalCode}) {
    return _Country(
      googleCountry: googleCountry,
      openStreetCountry: openStreetCountry,
      name: name,
      tld: tld,
      cca2: cca2,
      ccn3: ccn3,
      cca3: cca3,
      independent: independent,
      status: status,
      unMember: unMember,
      currencies: currencies,
      idd: idd,
      capital: capital,
      altSpellings: altSpellings,
      region: region,
      subregion: subregion,
      languages: languages,
      translations: translations,
      latlng: latlng,
      landlocked: landlocked,
      area: area,
      demonyms: demonyms,
      flag: flag,
      maps: maps,
      population: population,
      car: car,
      timezones: timezones,
      continents: continents,
      flags: flags,
      coatOfArms: coatOfArms,
      startOfWeek: startOfWeek,
      capitalInfo: capitalInfo,
      postalCode: postalCode,
    );
  }

  Country fromJson(Map<String, Object?> json) {
    return Country.fromJson(json);
  }
}

/// @nodoc
const $Country = _$CountryTearOff();

/// @nodoc
mixin _$Country {
  String? get googleCountry => throw _privateConstructorUsedError;
  String? get openStreetCountry => throw _privateConstructorUsedError;
  Name? get name => throw _privateConstructorUsedError;
  List<String>? get tld => throw _privateConstructorUsedError;
  String? get cca2 => throw _privateConstructorUsedError;
  String? get ccn3 => throw _privateConstructorUsedError;
  String? get cca3 => throw _privateConstructorUsedError;
  bool? get independent => throw _privateConstructorUsedError;
  String? get status => throw _privateConstructorUsedError;
  bool? get unMember => throw _privateConstructorUsedError;
  Currencies? get currencies => throw _privateConstructorUsedError;
  Idd? get idd => throw _privateConstructorUsedError;
  List<String>? get capital => throw _privateConstructorUsedError;
  List<String>? get altSpellings => throw _privateConstructorUsedError;
  String? get region => throw _privateConstructorUsedError;
  String? get subregion => throw _privateConstructorUsedError;
  Languages? get languages => throw _privateConstructorUsedError;
  Translations? get translations => throw _privateConstructorUsedError;
  List<double>? get latlng => throw _privateConstructorUsedError;
  bool? get landlocked => throw _privateConstructorUsedError;
  double? get area => throw _privateConstructorUsedError;
  Demonyms? get demonyms => throw _privateConstructorUsedError;
  String? get flag => throw _privateConstructorUsedError;
  Maps? get maps => throw _privateConstructorUsedError;
  int? get population => throw _privateConstructorUsedError;
  Map<dynamic, dynamic>? get car => throw _privateConstructorUsedError;
  List<String>? get timezones => throw _privateConstructorUsedError;
  List<String>? get continents => throw _privateConstructorUsedError;
  Flags? get flags => throw _privateConstructorUsedError;
  Flags? get coatOfArms => throw _privateConstructorUsedError;
  String? get startOfWeek => throw _privateConstructorUsedError;
  CapitalInfo? get capitalInfo => throw _privateConstructorUsedError;
  PostalCode? get postalCode => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CountryCopyWith<Country> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CountryCopyWith<$Res> {
  factory $CountryCopyWith(Country value, $Res Function(Country) then) =
      _$CountryCopyWithImpl<$Res>;
  $Res call(
      {String? googleCountry,
      String? openStreetCountry,
      Name? name,
      List<String>? tld,
      String? cca2,
      String? ccn3,
      String? cca3,
      bool? independent,
      String? status,
      bool? unMember,
      Currencies? currencies,
      Idd? idd,
      List<String>? capital,
      List<String>? altSpellings,
      String? region,
      String? subregion,
      Languages? languages,
      Translations? translations,
      List<double>? latlng,
      bool? landlocked,
      double? area,
      Demonyms? demonyms,
      String? flag,
      Maps? maps,
      int? population,
      Map<dynamic, dynamic>? car,
      List<String>? timezones,
      List<String>? continents,
      Flags? flags,
      Flags? coatOfArms,
      String? startOfWeek,
      CapitalInfo? capitalInfo,
      PostalCode? postalCode});

  $NameCopyWith<$Res>? get name;
  $CurrenciesCopyWith<$Res>? get currencies;
  $IddCopyWith<$Res>? get idd;
  $LanguagesCopyWith<$Res>? get languages;
  $TranslationsCopyWith<$Res>? get translations;
  $DemonymsCopyWith<$Res>? get demonyms;
  $MapsCopyWith<$Res>? get maps;
  $FlagsCopyWith<$Res>? get flags;
  $FlagsCopyWith<$Res>? get coatOfArms;
  $CapitalInfoCopyWith<$Res>? get capitalInfo;
  $PostalCodeCopyWith<$Res>? get postalCode;
}

/// @nodoc
class _$CountryCopyWithImpl<$Res> implements $CountryCopyWith<$Res> {
  _$CountryCopyWithImpl(this._value, this._then);

  final Country _value;
  // ignore: unused_field
  final $Res Function(Country) _then;

  @override
  $Res call({
    Object? googleCountry = freezed,
    Object? openStreetCountry = freezed,
    Object? name = freezed,
    Object? tld = freezed,
    Object? cca2 = freezed,
    Object? ccn3 = freezed,
    Object? cca3 = freezed,
    Object? independent = freezed,
    Object? status = freezed,
    Object? unMember = freezed,
    Object? currencies = freezed,
    Object? idd = freezed,
    Object? capital = freezed,
    Object? altSpellings = freezed,
    Object? region = freezed,
    Object? subregion = freezed,
    Object? languages = freezed,
    Object? translations = freezed,
    Object? latlng = freezed,
    Object? landlocked = freezed,
    Object? area = freezed,
    Object? demonyms = freezed,
    Object? flag = freezed,
    Object? maps = freezed,
    Object? population = freezed,
    Object? car = freezed,
    Object? timezones = freezed,
    Object? continents = freezed,
    Object? flags = freezed,
    Object? coatOfArms = freezed,
    Object? startOfWeek = freezed,
    Object? capitalInfo = freezed,
    Object? postalCode = freezed,
  }) {
    return _then(_value.copyWith(
      googleCountry: googleCountry == freezed
          ? _value.googleCountry
          : googleCountry // ignore: cast_nullable_to_non_nullable
              as String?,
      openStreetCountry: openStreetCountry == freezed
          ? _value.openStreetCountry
          : openStreetCountry // ignore: cast_nullable_to_non_nullable
              as String?,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as Name?,
      tld: tld == freezed
          ? _value.tld
          : tld // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      cca2: cca2 == freezed
          ? _value.cca2
          : cca2 // ignore: cast_nullable_to_non_nullable
              as String?,
      ccn3: ccn3 == freezed
          ? _value.ccn3
          : ccn3 // ignore: cast_nullable_to_non_nullable
              as String?,
      cca3: cca3 == freezed
          ? _value.cca3
          : cca3 // ignore: cast_nullable_to_non_nullable
              as String?,
      independent: independent == freezed
          ? _value.independent
          : independent // ignore: cast_nullable_to_non_nullable
              as bool?,
      status: status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String?,
      unMember: unMember == freezed
          ? _value.unMember
          : unMember // ignore: cast_nullable_to_non_nullable
              as bool?,
      currencies: currencies == freezed
          ? _value.currencies
          : currencies // ignore: cast_nullable_to_non_nullable
              as Currencies?,
      idd: idd == freezed
          ? _value.idd
          : idd // ignore: cast_nullable_to_non_nullable
              as Idd?,
      capital: capital == freezed
          ? _value.capital
          : capital // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      altSpellings: altSpellings == freezed
          ? _value.altSpellings
          : altSpellings // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      region: region == freezed
          ? _value.region
          : region // ignore: cast_nullable_to_non_nullable
              as String?,
      subregion: subregion == freezed
          ? _value.subregion
          : subregion // ignore: cast_nullable_to_non_nullable
              as String?,
      languages: languages == freezed
          ? _value.languages
          : languages // ignore: cast_nullable_to_non_nullable
              as Languages?,
      translations: translations == freezed
          ? _value.translations
          : translations // ignore: cast_nullable_to_non_nullable
              as Translations?,
      latlng: latlng == freezed
          ? _value.latlng
          : latlng // ignore: cast_nullable_to_non_nullable
              as List<double>?,
      landlocked: landlocked == freezed
          ? _value.landlocked
          : landlocked // ignore: cast_nullable_to_non_nullable
              as bool?,
      area: area == freezed
          ? _value.area
          : area // ignore: cast_nullable_to_non_nullable
              as double?,
      demonyms: demonyms == freezed
          ? _value.demonyms
          : demonyms // ignore: cast_nullable_to_non_nullable
              as Demonyms?,
      flag: flag == freezed
          ? _value.flag
          : flag // ignore: cast_nullable_to_non_nullable
              as String?,
      maps: maps == freezed
          ? _value.maps
          : maps // ignore: cast_nullable_to_non_nullable
              as Maps?,
      population: population == freezed
          ? _value.population
          : population // ignore: cast_nullable_to_non_nullable
              as int?,
      car: car == freezed
          ? _value.car
          : car // ignore: cast_nullable_to_non_nullable
              as Map<dynamic, dynamic>?,
      timezones: timezones == freezed
          ? _value.timezones
          : timezones // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      continents: continents == freezed
          ? _value.continents
          : continents // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      flags: flags == freezed
          ? _value.flags
          : flags // ignore: cast_nullable_to_non_nullable
              as Flags?,
      coatOfArms: coatOfArms == freezed
          ? _value.coatOfArms
          : coatOfArms // ignore: cast_nullable_to_non_nullable
              as Flags?,
      startOfWeek: startOfWeek == freezed
          ? _value.startOfWeek
          : startOfWeek // ignore: cast_nullable_to_non_nullable
              as String?,
      capitalInfo: capitalInfo == freezed
          ? _value.capitalInfo
          : capitalInfo // ignore: cast_nullable_to_non_nullable
              as CapitalInfo?,
      postalCode: postalCode == freezed
          ? _value.postalCode
          : postalCode // ignore: cast_nullable_to_non_nullable
              as PostalCode?,
    ));
  }

  @override
  $NameCopyWith<$Res>? get name {
    if (_value.name == null) {
      return null;
    }

    return $NameCopyWith<$Res>(_value.name!, (value) {
      return _then(_value.copyWith(name: value));
    });
  }

  @override
  $CurrenciesCopyWith<$Res>? get currencies {
    if (_value.currencies == null) {
      return null;
    }

    return $CurrenciesCopyWith<$Res>(_value.currencies!, (value) {
      return _then(_value.copyWith(currencies: value));
    });
  }

  @override
  $IddCopyWith<$Res>? get idd {
    if (_value.idd == null) {
      return null;
    }

    return $IddCopyWith<$Res>(_value.idd!, (value) {
      return _then(_value.copyWith(idd: value));
    });
  }

  @override
  $LanguagesCopyWith<$Res>? get languages {
    if (_value.languages == null) {
      return null;
    }

    return $LanguagesCopyWith<$Res>(_value.languages!, (value) {
      return _then(_value.copyWith(languages: value));
    });
  }

  @override
  $TranslationsCopyWith<$Res>? get translations {
    if (_value.translations == null) {
      return null;
    }

    return $TranslationsCopyWith<$Res>(_value.translations!, (value) {
      return _then(_value.copyWith(translations: value));
    });
  }

  @override
  $DemonymsCopyWith<$Res>? get demonyms {
    if (_value.demonyms == null) {
      return null;
    }

    return $DemonymsCopyWith<$Res>(_value.demonyms!, (value) {
      return _then(_value.copyWith(demonyms: value));
    });
  }

  @override
  $MapsCopyWith<$Res>? get maps {
    if (_value.maps == null) {
      return null;
    }

    return $MapsCopyWith<$Res>(_value.maps!, (value) {
      return _then(_value.copyWith(maps: value));
    });
  }

  @override
  $FlagsCopyWith<$Res>? get flags {
    if (_value.flags == null) {
      return null;
    }

    return $FlagsCopyWith<$Res>(_value.flags!, (value) {
      return _then(_value.copyWith(flags: value));
    });
  }

  @override
  $FlagsCopyWith<$Res>? get coatOfArms {
    if (_value.coatOfArms == null) {
      return null;
    }

    return $FlagsCopyWith<$Res>(_value.coatOfArms!, (value) {
      return _then(_value.copyWith(coatOfArms: value));
    });
  }

  @override
  $CapitalInfoCopyWith<$Res>? get capitalInfo {
    if (_value.capitalInfo == null) {
      return null;
    }

    return $CapitalInfoCopyWith<$Res>(_value.capitalInfo!, (value) {
      return _then(_value.copyWith(capitalInfo: value));
    });
  }

  @override
  $PostalCodeCopyWith<$Res>? get postalCode {
    if (_value.postalCode == null) {
      return null;
    }

    return $PostalCodeCopyWith<$Res>(_value.postalCode!, (value) {
      return _then(_value.copyWith(postalCode: value));
    });
  }
}

/// @nodoc
abstract class _$CountryCopyWith<$Res> implements $CountryCopyWith<$Res> {
  factory _$CountryCopyWith(_Country value, $Res Function(_Country) then) =
      __$CountryCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? googleCountry,
      String? openStreetCountry,
      Name? name,
      List<String>? tld,
      String? cca2,
      String? ccn3,
      String? cca3,
      bool? independent,
      String? status,
      bool? unMember,
      Currencies? currencies,
      Idd? idd,
      List<String>? capital,
      List<String>? altSpellings,
      String? region,
      String? subregion,
      Languages? languages,
      Translations? translations,
      List<double>? latlng,
      bool? landlocked,
      double? area,
      Demonyms? demonyms,
      String? flag,
      Maps? maps,
      int? population,
      Map<dynamic, dynamic>? car,
      List<String>? timezones,
      List<String>? continents,
      Flags? flags,
      Flags? coatOfArms,
      String? startOfWeek,
      CapitalInfo? capitalInfo,
      PostalCode? postalCode});

  @override
  $NameCopyWith<$Res>? get name;
  @override
  $CurrenciesCopyWith<$Res>? get currencies;
  @override
  $IddCopyWith<$Res>? get idd;
  @override
  $LanguagesCopyWith<$Res>? get languages;
  @override
  $TranslationsCopyWith<$Res>? get translations;
  @override
  $DemonymsCopyWith<$Res>? get demonyms;
  @override
  $MapsCopyWith<$Res>? get maps;
  @override
  $FlagsCopyWith<$Res>? get flags;
  @override
  $FlagsCopyWith<$Res>? get coatOfArms;
  @override
  $CapitalInfoCopyWith<$Res>? get capitalInfo;
  @override
  $PostalCodeCopyWith<$Res>? get postalCode;
}

/// @nodoc
class __$CountryCopyWithImpl<$Res> extends _$CountryCopyWithImpl<$Res>
    implements _$CountryCopyWith<$Res> {
  __$CountryCopyWithImpl(_Country _value, $Res Function(_Country) _then)
      : super(_value, (v) => _then(v as _Country));

  @override
  _Country get _value => super._value as _Country;

  @override
  $Res call({
    Object? googleCountry = freezed,
    Object? openStreetCountry = freezed,
    Object? name = freezed,
    Object? tld = freezed,
    Object? cca2 = freezed,
    Object? ccn3 = freezed,
    Object? cca3 = freezed,
    Object? independent = freezed,
    Object? status = freezed,
    Object? unMember = freezed,
    Object? currencies = freezed,
    Object? idd = freezed,
    Object? capital = freezed,
    Object? altSpellings = freezed,
    Object? region = freezed,
    Object? subregion = freezed,
    Object? languages = freezed,
    Object? translations = freezed,
    Object? latlng = freezed,
    Object? landlocked = freezed,
    Object? area = freezed,
    Object? demonyms = freezed,
    Object? flag = freezed,
    Object? maps = freezed,
    Object? population = freezed,
    Object? car = freezed,
    Object? timezones = freezed,
    Object? continents = freezed,
    Object? flags = freezed,
    Object? coatOfArms = freezed,
    Object? startOfWeek = freezed,
    Object? capitalInfo = freezed,
    Object? postalCode = freezed,
  }) {
    return _then(_Country(
      googleCountry: googleCountry == freezed
          ? _value.googleCountry
          : googleCountry // ignore: cast_nullable_to_non_nullable
              as String?,
      openStreetCountry: openStreetCountry == freezed
          ? _value.openStreetCountry
          : openStreetCountry // ignore: cast_nullable_to_non_nullable
              as String?,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as Name?,
      tld: tld == freezed
          ? _value.tld
          : tld // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      cca2: cca2 == freezed
          ? _value.cca2
          : cca2 // ignore: cast_nullable_to_non_nullable
              as String?,
      ccn3: ccn3 == freezed
          ? _value.ccn3
          : ccn3 // ignore: cast_nullable_to_non_nullable
              as String?,
      cca3: cca3 == freezed
          ? _value.cca3
          : cca3 // ignore: cast_nullable_to_non_nullable
              as String?,
      independent: independent == freezed
          ? _value.independent
          : independent // ignore: cast_nullable_to_non_nullable
              as bool?,
      status: status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String?,
      unMember: unMember == freezed
          ? _value.unMember
          : unMember // ignore: cast_nullable_to_non_nullable
              as bool?,
      currencies: currencies == freezed
          ? _value.currencies
          : currencies // ignore: cast_nullable_to_non_nullable
              as Currencies?,
      idd: idd == freezed
          ? _value.idd
          : idd // ignore: cast_nullable_to_non_nullable
              as Idd?,
      capital: capital == freezed
          ? _value.capital
          : capital // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      altSpellings: altSpellings == freezed
          ? _value.altSpellings
          : altSpellings // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      region: region == freezed
          ? _value.region
          : region // ignore: cast_nullable_to_non_nullable
              as String?,
      subregion: subregion == freezed
          ? _value.subregion
          : subregion // ignore: cast_nullable_to_non_nullable
              as String?,
      languages: languages == freezed
          ? _value.languages
          : languages // ignore: cast_nullable_to_non_nullable
              as Languages?,
      translations: translations == freezed
          ? _value.translations
          : translations // ignore: cast_nullable_to_non_nullable
              as Translations?,
      latlng: latlng == freezed
          ? _value.latlng
          : latlng // ignore: cast_nullable_to_non_nullable
              as List<double>?,
      landlocked: landlocked == freezed
          ? _value.landlocked
          : landlocked // ignore: cast_nullable_to_non_nullable
              as bool?,
      area: area == freezed
          ? _value.area
          : area // ignore: cast_nullable_to_non_nullable
              as double?,
      demonyms: demonyms == freezed
          ? _value.demonyms
          : demonyms // ignore: cast_nullable_to_non_nullable
              as Demonyms?,
      flag: flag == freezed
          ? _value.flag
          : flag // ignore: cast_nullable_to_non_nullable
              as String?,
      maps: maps == freezed
          ? _value.maps
          : maps // ignore: cast_nullable_to_non_nullable
              as Maps?,
      population: population == freezed
          ? _value.population
          : population // ignore: cast_nullable_to_non_nullable
              as int?,
      car: car == freezed
          ? _value.car
          : car // ignore: cast_nullable_to_non_nullable
              as Map<dynamic, dynamic>?,
      timezones: timezones == freezed
          ? _value.timezones
          : timezones // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      continents: continents == freezed
          ? _value.continents
          : continents // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      flags: flags == freezed
          ? _value.flags
          : flags // ignore: cast_nullable_to_non_nullable
              as Flags?,
      coatOfArms: coatOfArms == freezed
          ? _value.coatOfArms
          : coatOfArms // ignore: cast_nullable_to_non_nullable
              as Flags?,
      startOfWeek: startOfWeek == freezed
          ? _value.startOfWeek
          : startOfWeek // ignore: cast_nullable_to_non_nullable
              as String?,
      capitalInfo: capitalInfo == freezed
          ? _value.capitalInfo
          : capitalInfo // ignore: cast_nullable_to_non_nullable
              as CapitalInfo?,
      postalCode: postalCode == freezed
          ? _value.postalCode
          : postalCode // ignore: cast_nullable_to_non_nullable
              as PostalCode?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Country extends _Country {
  const _$_Country(
      {required this.googleCountry,
      required this.openStreetCountry,
      required this.name,
      required this.tld,
      required this.cca2,
      required this.ccn3,
      required this.cca3,
      required this.independent,
      required this.status,
      required this.unMember,
      required this.currencies,
      required this.idd,
      required this.capital,
      required this.altSpellings,
      required this.region,
      required this.subregion,
      required this.languages,
      required this.translations,
      required this.latlng,
      required this.landlocked,
      required this.area,
      required this.demonyms,
      required this.flag,
      required this.maps,
      required this.population,
      required this.car,
      required this.timezones,
      required this.continents,
      required this.flags,
      required this.coatOfArms,
      required this.startOfWeek,
      required this.capitalInfo,
      required this.postalCode})
      : super._();

  factory _$_Country.fromJson(Map<String, dynamic> json) =>
      _$$_CountryFromJson(json);

  @override
  final String? googleCountry;
  @override
  final String? openStreetCountry;
  @override
  final Name? name;
  @override
  final List<String>? tld;
  @override
  final String? cca2;
  @override
  final String? ccn3;
  @override
  final String? cca3;
  @override
  final bool? independent;
  @override
  final String? status;
  @override
  final bool? unMember;
  @override
  final Currencies? currencies;
  @override
  final Idd? idd;
  @override
  final List<String>? capital;
  @override
  final List<String>? altSpellings;
  @override
  final String? region;
  @override
  final String? subregion;
  @override
  final Languages? languages;
  @override
  final Translations? translations;
  @override
  final List<double>? latlng;
  @override
  final bool? landlocked;
  @override
  final double? area;
  @override
  final Demonyms? demonyms;
  @override
  final String? flag;
  @override
  final Maps? maps;
  @override
  final int? population;
  @override
  final Map<dynamic, dynamic>? car;
  @override
  final List<String>? timezones;
  @override
  final List<String>? continents;
  @override
  final Flags? flags;
  @override
  final Flags? coatOfArms;
  @override
  final String? startOfWeek;
  @override
  final CapitalInfo? capitalInfo;
  @override
  final PostalCode? postalCode;

  @override
  String toString() {
    return 'Country(googleCountry: $googleCountry, openStreetCountry: $openStreetCountry, name: $name, tld: $tld, cca2: $cca2, ccn3: $ccn3, cca3: $cca3, independent: $independent, status: $status, unMember: $unMember, currencies: $currencies, idd: $idd, capital: $capital, altSpellings: $altSpellings, region: $region, subregion: $subregion, languages: $languages, translations: $translations, latlng: $latlng, landlocked: $landlocked, area: $area, demonyms: $demonyms, flag: $flag, maps: $maps, population: $population, car: $car, timezones: $timezones, continents: $continents, flags: $flags, coatOfArms: $coatOfArms, startOfWeek: $startOfWeek, capitalInfo: $capitalInfo, postalCode: $postalCode)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Country &&
            const DeepCollectionEquality()
                .equals(other.googleCountry, googleCountry) &&
            const DeepCollectionEquality()
                .equals(other.openStreetCountry, openStreetCountry) &&
            const DeepCollectionEquality().equals(other.name, name) &&
            const DeepCollectionEquality().equals(other.tld, tld) &&
            const DeepCollectionEquality().equals(other.cca2, cca2) &&
            const DeepCollectionEquality().equals(other.ccn3, ccn3) &&
            const DeepCollectionEquality().equals(other.cca3, cca3) &&
            const DeepCollectionEquality()
                .equals(other.independent, independent) &&
            const DeepCollectionEquality().equals(other.status, status) &&
            const DeepCollectionEquality().equals(other.unMember, unMember) &&
            const DeepCollectionEquality()
                .equals(other.currencies, currencies) &&
            const DeepCollectionEquality().equals(other.idd, idd) &&
            const DeepCollectionEquality().equals(other.capital, capital) &&
            const DeepCollectionEquality()
                .equals(other.altSpellings, altSpellings) &&
            const DeepCollectionEquality().equals(other.region, region) &&
            const DeepCollectionEquality().equals(other.subregion, subregion) &&
            const DeepCollectionEquality().equals(other.languages, languages) &&
            const DeepCollectionEquality()
                .equals(other.translations, translations) &&
            const DeepCollectionEquality().equals(other.latlng, latlng) &&
            const DeepCollectionEquality()
                .equals(other.landlocked, landlocked) &&
            const DeepCollectionEquality().equals(other.area, area) &&
            const DeepCollectionEquality().equals(other.demonyms, demonyms) &&
            const DeepCollectionEquality().equals(other.flag, flag) &&
            const DeepCollectionEquality().equals(other.maps, maps) &&
            const DeepCollectionEquality()
                .equals(other.population, population) &&
            const DeepCollectionEquality().equals(other.car, car) &&
            const DeepCollectionEquality().equals(other.timezones, timezones) &&
            const DeepCollectionEquality()
                .equals(other.continents, continents) &&
            const DeepCollectionEquality().equals(other.flags, flags) &&
            const DeepCollectionEquality()
                .equals(other.coatOfArms, coatOfArms) &&
            const DeepCollectionEquality()
                .equals(other.startOfWeek, startOfWeek) &&
            const DeepCollectionEquality()
                .equals(other.capitalInfo, capitalInfo) &&
            const DeepCollectionEquality()
                .equals(other.postalCode, postalCode));
  }

  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        const DeepCollectionEquality().hash(googleCountry),
        const DeepCollectionEquality().hash(openStreetCountry),
        const DeepCollectionEquality().hash(name),
        const DeepCollectionEquality().hash(tld),
        const DeepCollectionEquality().hash(cca2),
        const DeepCollectionEquality().hash(ccn3),
        const DeepCollectionEquality().hash(cca3),
        const DeepCollectionEquality().hash(independent),
        const DeepCollectionEquality().hash(status),
        const DeepCollectionEquality().hash(unMember),
        const DeepCollectionEquality().hash(currencies),
        const DeepCollectionEquality().hash(idd),
        const DeepCollectionEquality().hash(capital),
        const DeepCollectionEquality().hash(altSpellings),
        const DeepCollectionEquality().hash(region),
        const DeepCollectionEquality().hash(subregion),
        const DeepCollectionEquality().hash(languages),
        const DeepCollectionEquality().hash(translations),
        const DeepCollectionEquality().hash(latlng),
        const DeepCollectionEquality().hash(landlocked),
        const DeepCollectionEquality().hash(area),
        const DeepCollectionEquality().hash(demonyms),
        const DeepCollectionEquality().hash(flag),
        const DeepCollectionEquality().hash(maps),
        const DeepCollectionEquality().hash(population),
        const DeepCollectionEquality().hash(car),
        const DeepCollectionEquality().hash(timezones),
        const DeepCollectionEquality().hash(continents),
        const DeepCollectionEquality().hash(flags),
        const DeepCollectionEquality().hash(coatOfArms),
        const DeepCollectionEquality().hash(startOfWeek),
        const DeepCollectionEquality().hash(capitalInfo),
        const DeepCollectionEquality().hash(postalCode)
      ]);

  @JsonKey(ignore: true)
  @override
  _$CountryCopyWith<_Country> get copyWith =>
      __$CountryCopyWithImpl<_Country>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CountryToJson(this);
  }
}

abstract class _Country extends Country {
  const factory _Country(
      {required String? googleCountry,
      required String? openStreetCountry,
      required Name? name,
      required List<String>? tld,
      required String? cca2,
      required String? ccn3,
      required String? cca3,
      required bool? independent,
      required String? status,
      required bool? unMember,
      required Currencies? currencies,
      required Idd? idd,
      required List<String>? capital,
      required List<String>? altSpellings,
      required String? region,
      required String? subregion,
      required Languages? languages,
      required Translations? translations,
      required List<double>? latlng,
      required bool? landlocked,
      required double? area,
      required Demonyms? demonyms,
      required String? flag,
      required Maps? maps,
      required int? population,
      required Map<dynamic, dynamic>? car,
      required List<String>? timezones,
      required List<String>? continents,
      required Flags? flags,
      required Flags? coatOfArms,
      required String? startOfWeek,
      required CapitalInfo? capitalInfo,
      required PostalCode? postalCode}) = _$_Country;
  const _Country._() : super._();

  factory _Country.fromJson(Map<String, dynamic> json) = _$_Country.fromJson;

  @override
  String? get googleCountry;
  @override
  String? get openStreetCountry;
  @override
  Name? get name;
  @override
  List<String>? get tld;
  @override
  String? get cca2;
  @override
  String? get ccn3;
  @override
  String? get cca3;
  @override
  bool? get independent;
  @override
  String? get status;
  @override
  bool? get unMember;
  @override
  Currencies? get currencies;
  @override
  Idd? get idd;
  @override
  List<String>? get capital;
  @override
  List<String>? get altSpellings;
  @override
  String? get region;
  @override
  String? get subregion;
  @override
  Languages? get languages;
  @override
  Translations? get translations;
  @override
  List<double>? get latlng;
  @override
  bool? get landlocked;
  @override
  double? get area;
  @override
  Demonyms? get demonyms;
  @override
  String? get flag;
  @override
  Maps? get maps;
  @override
  int? get population;
  @override
  Map<dynamic, dynamic>? get car;
  @override
  List<String>? get timezones;
  @override
  List<String>? get continents;
  @override
  Flags? get flags;
  @override
  Flags? get coatOfArms;
  @override
  String? get startOfWeek;
  @override
  CapitalInfo? get capitalInfo;
  @override
  PostalCode? get postalCode;
  @override
  @JsonKey(ignore: true)
  _$CountryCopyWith<_Country> get copyWith =>
      throw _privateConstructorUsedError;
}
