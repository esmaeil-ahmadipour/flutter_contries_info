import 'package:freezed_annotation/freezed_annotation.dart' ;
part 'maps.g.dart';
part 'maps.freezed.dart';

@freezed
class Maps with _$Maps {
  const Maps._();
  const factory Maps({
    required String? googleMaps,
   required String? openStreetMaps,

  }) = _Maps;
  factory Maps.fromJson(Map<String, dynamic> json) => _$MapsFromJson(json);
}
