// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'maps.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Maps _$MapsFromJson(Map<String, dynamic> json) {
  return _Maps.fromJson(json);
}

/// @nodoc
class _$MapsTearOff {
  const _$MapsTearOff();

  _Maps call({required String? googleMaps, required String? openStreetMaps}) {
    return _Maps(
      googleMaps: googleMaps,
      openStreetMaps: openStreetMaps,
    );
  }

  Maps fromJson(Map<String, Object?> json) {
    return Maps.fromJson(json);
  }
}

/// @nodoc
const $Maps = _$MapsTearOff();

/// @nodoc
mixin _$Maps {
  String? get googleMaps => throw _privateConstructorUsedError;
  String? get openStreetMaps => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MapsCopyWith<Maps> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MapsCopyWith<$Res> {
  factory $MapsCopyWith(Maps value, $Res Function(Maps) then) =
      _$MapsCopyWithImpl<$Res>;
  $Res call({String? googleMaps, String? openStreetMaps});
}

/// @nodoc
class _$MapsCopyWithImpl<$Res> implements $MapsCopyWith<$Res> {
  _$MapsCopyWithImpl(this._value, this._then);

  final Maps _value;
  // ignore: unused_field
  final $Res Function(Maps) _then;

  @override
  $Res call({
    Object? googleMaps = freezed,
    Object? openStreetMaps = freezed,
  }) {
    return _then(_value.copyWith(
      googleMaps: googleMaps == freezed
          ? _value.googleMaps
          : googleMaps // ignore: cast_nullable_to_non_nullable
              as String?,
      openStreetMaps: openStreetMaps == freezed
          ? _value.openStreetMaps
          : openStreetMaps // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$MapsCopyWith<$Res> implements $MapsCopyWith<$Res> {
  factory _$MapsCopyWith(_Maps value, $Res Function(_Maps) then) =
      __$MapsCopyWithImpl<$Res>;
  @override
  $Res call({String? googleMaps, String? openStreetMaps});
}

/// @nodoc
class __$MapsCopyWithImpl<$Res> extends _$MapsCopyWithImpl<$Res>
    implements _$MapsCopyWith<$Res> {
  __$MapsCopyWithImpl(_Maps _value, $Res Function(_Maps) _then)
      : super(_value, (v) => _then(v as _Maps));

  @override
  _Maps get _value => super._value as _Maps;

  @override
  $Res call({
    Object? googleMaps = freezed,
    Object? openStreetMaps = freezed,
  }) {
    return _then(_Maps(
      googleMaps: googleMaps == freezed
          ? _value.googleMaps
          : googleMaps // ignore: cast_nullable_to_non_nullable
              as String?,
      openStreetMaps: openStreetMaps == freezed
          ? _value.openStreetMaps
          : openStreetMaps // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Maps extends _Maps {
  const _$_Maps({required this.googleMaps, required this.openStreetMaps})
      : super._();

  factory _$_Maps.fromJson(Map<String, dynamic> json) => _$$_MapsFromJson(json);

  @override
  final String? googleMaps;
  @override
  final String? openStreetMaps;

  @override
  String toString() {
    return 'Maps(googleMaps: $googleMaps, openStreetMaps: $openStreetMaps)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Maps &&
            const DeepCollectionEquality()
                .equals(other.googleMaps, googleMaps) &&
            const DeepCollectionEquality()
                .equals(other.openStreetMaps, openStreetMaps));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(googleMaps),
      const DeepCollectionEquality().hash(openStreetMaps));

  @JsonKey(ignore: true)
  @override
  _$MapsCopyWith<_Maps> get copyWith =>
      __$MapsCopyWithImpl<_Maps>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_MapsToJson(this);
  }
}

abstract class _Maps extends Maps {
  const factory _Maps(
      {required String? googleMaps, required String? openStreetMaps}) = _$_Maps;
  const _Maps._() : super._();

  factory _Maps.fromJson(Map<String, dynamic> json) = _$_Maps.fromJson;

  @override
  String? get googleMaps;
  @override
  String? get openStreetMaps;
  @override
  @JsonKey(ignore: true)
  _$MapsCopyWith<_Maps> get copyWith => throw _privateConstructorUsedError;
}
