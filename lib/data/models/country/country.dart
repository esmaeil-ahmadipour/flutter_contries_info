import 'package:flutter_countries_info/data/models/country/capitalInfo/capitalInfo.dart';
import 'package:flutter_countries_info/data/models/country/currencies/currencies.dart';
import 'package:flutter_countries_info/data/models/country/demonyms/demonyms.dart';
import 'package:flutter_countries_info/data/models/country/flags/flags.dart';
import 'package:flutter_countries_info/data/models/country/idd/idd.dart';
import 'package:flutter_countries_info/data/models/country/languages/languages.dart';
import 'package:flutter_countries_info/data/models/country/maps/maps.dart';
import 'package:flutter_countries_info/data/models/country/name/name.dart';
import 'package:flutter_countries_info/data/models/country/postal_code/postalCode.dart';
import 'package:flutter_countries_info/data/models/country/translations/translations.dart';
import 'package:freezed_annotation/freezed_annotation.dart' ;
part 'country.g.dart';
part 'country.freezed.dart';

@freezed
class Country with _$Country {
  const Country._();
  const factory Country({
    required String? googleCountry,
    required String? openStreetCountry,

    required
    Name? name,required
    List<String>? tld,required
    String? cca2,required
    String? ccn3,required
    String? cca3,required
    bool? independent,required
    String? status,required
    bool? unMember,required
    Currencies? currencies,required
    Idd? idd,required
    List<String>? capital,required
    List<String>? altSpellings,required
    String? region,required
    String? subregion,required
    Languages? languages,required
    Translations? translations,required
    List<double>? latlng,required
    bool? landlocked,required
    double? area,required
    Demonyms? demonyms,required
    String? flag,required
    Maps? maps,required
    int? population,required
    Map? car,required
    List<String>? timezones,required
    List<String>? continents,required
    Flags? flags,required
    Flags? coatOfArms,required
    String? startOfWeek,required
    CapitalInfo? capitalInfo,required
    PostalCode? postalCode,


  }) = _Country;
  factory Country.fromJson(Map<String, dynamic> json) => _$CountryFromJson(json);
}
