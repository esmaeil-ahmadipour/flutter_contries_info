import 'package:freezed_annotation/freezed_annotation.dart' ;
part 'languages.g.dart';
part 'languages.freezed.dart';

@freezed
class Languages with _$Languages {
  const Languages._();
  const factory Languages({
    required String? kal,

  }) = _Languages;
  factory Languages.fromJson(Map<String, dynamic> json) => _$LanguagesFromJson(json);
}
