// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'languages.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Languages _$$_LanguagesFromJson(Map<String, dynamic> json) => _$_Languages(
      kal: json['kal'] as String?,
    );

Map<String, dynamic> _$$_LanguagesToJson(_$_Languages instance) =>
    <String, dynamic>{
      'kal': instance.kal,
    };
