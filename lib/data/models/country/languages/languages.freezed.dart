// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'languages.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Languages _$LanguagesFromJson(Map<String, dynamic> json) {
  return _Languages.fromJson(json);
}

/// @nodoc
class _$LanguagesTearOff {
  const _$LanguagesTearOff();

  _Languages call({required String? kal}) {
    return _Languages(
      kal: kal,
    );
  }

  Languages fromJson(Map<String, Object?> json) {
    return Languages.fromJson(json);
  }
}

/// @nodoc
const $Languages = _$LanguagesTearOff();

/// @nodoc
mixin _$Languages {
  String? get kal => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LanguagesCopyWith<Languages> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LanguagesCopyWith<$Res> {
  factory $LanguagesCopyWith(Languages value, $Res Function(Languages) then) =
      _$LanguagesCopyWithImpl<$Res>;
  $Res call({String? kal});
}

/// @nodoc
class _$LanguagesCopyWithImpl<$Res> implements $LanguagesCopyWith<$Res> {
  _$LanguagesCopyWithImpl(this._value, this._then);

  final Languages _value;
  // ignore: unused_field
  final $Res Function(Languages) _then;

  @override
  $Res call({
    Object? kal = freezed,
  }) {
    return _then(_value.copyWith(
      kal: kal == freezed
          ? _value.kal
          : kal // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$LanguagesCopyWith<$Res> implements $LanguagesCopyWith<$Res> {
  factory _$LanguagesCopyWith(
          _Languages value, $Res Function(_Languages) then) =
      __$LanguagesCopyWithImpl<$Res>;
  @override
  $Res call({String? kal});
}

/// @nodoc
class __$LanguagesCopyWithImpl<$Res> extends _$LanguagesCopyWithImpl<$Res>
    implements _$LanguagesCopyWith<$Res> {
  __$LanguagesCopyWithImpl(_Languages _value, $Res Function(_Languages) _then)
      : super(_value, (v) => _then(v as _Languages));

  @override
  _Languages get _value => super._value as _Languages;

  @override
  $Res call({
    Object? kal = freezed,
  }) {
    return _then(_Languages(
      kal: kal == freezed
          ? _value.kal
          : kal // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Languages extends _Languages {
  const _$_Languages({required this.kal}) : super._();

  factory _$_Languages.fromJson(Map<String, dynamic> json) =>
      _$$_LanguagesFromJson(json);

  @override
  final String? kal;

  @override
  String toString() {
    return 'Languages(kal: $kal)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Languages &&
            const DeepCollectionEquality().equals(other.kal, kal));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(kal));

  @JsonKey(ignore: true)
  @override
  _$LanguagesCopyWith<_Languages> get copyWith =>
      __$LanguagesCopyWithImpl<_Languages>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_LanguagesToJson(this);
  }
}

abstract class _Languages extends Languages {
  const factory _Languages({required String? kal}) = _$_Languages;
  const _Languages._() : super._();

  factory _Languages.fromJson(Map<String, dynamic> json) =
      _$_Languages.fromJson;

  @override
  String? get kal;
  @override
  @JsonKey(ignore: true)
  _$LanguagesCopyWith<_Languages> get copyWith =>
      throw _privateConstructorUsedError;
}
