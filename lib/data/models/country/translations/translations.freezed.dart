// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'translations.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Translations _$TranslationsFromJson(Map<String, dynamic> json) {
  return _Translations.fromJson(json);
}

/// @nodoc
class _$TranslationsTearOff {
  const _$TranslationsTearOff();

  _Translations call(
      {required Kal? ara,
      required Kal? ces,
      required Kal? cym,
      required Kal? deu,
      required Kal? est,
      required Kal? fin,
      required Eng? fra,
      required Kal? hrv,
      required Kal? hun,
      required Kal? ita,
      required Kal? jpn,
      required Kal? kor,
      required Kal? nld,
      required Kal? per,
      required Kal? pol,
      required Kal? por,
      required Kal? rus,
      required Kal? slk,
      required Kal? spa,
      required Kal? swe,
      required Kal? urd,
      required Kal? zho}) {
    return _Translations(
      ara: ara,
      ces: ces,
      cym: cym,
      deu: deu,
      est: est,
      fin: fin,
      fra: fra,
      hrv: hrv,
      hun: hun,
      ita: ita,
      jpn: jpn,
      kor: kor,
      nld: nld,
      per: per,
      pol: pol,
      por: por,
      rus: rus,
      slk: slk,
      spa: spa,
      swe: swe,
      urd: urd,
      zho: zho,
    );
  }

  Translations fromJson(Map<String, Object?> json) {
    return Translations.fromJson(json);
  }
}

/// @nodoc
const $Translations = _$TranslationsTearOff();

/// @nodoc
mixin _$Translations {
  Kal? get ara => throw _privateConstructorUsedError;
  Kal? get ces => throw _privateConstructorUsedError;
  Kal? get cym => throw _privateConstructorUsedError;
  Kal? get deu => throw _privateConstructorUsedError;
  Kal? get est => throw _privateConstructorUsedError;
  Kal? get fin => throw _privateConstructorUsedError;
  Eng? get fra => throw _privateConstructorUsedError;
  Kal? get hrv => throw _privateConstructorUsedError;
  Kal? get hun => throw _privateConstructorUsedError;
  Kal? get ita => throw _privateConstructorUsedError;
  Kal? get jpn => throw _privateConstructorUsedError;
  Kal? get kor => throw _privateConstructorUsedError;
  Kal? get nld => throw _privateConstructorUsedError;
  Kal? get per => throw _privateConstructorUsedError;
  Kal? get pol => throw _privateConstructorUsedError;
  Kal? get por => throw _privateConstructorUsedError;
  Kal? get rus => throw _privateConstructorUsedError;
  Kal? get slk => throw _privateConstructorUsedError;
  Kal? get spa => throw _privateConstructorUsedError;
  Kal? get swe => throw _privateConstructorUsedError;
  Kal? get urd => throw _privateConstructorUsedError;
  Kal? get zho => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TranslationsCopyWith<Translations> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TranslationsCopyWith<$Res> {
  factory $TranslationsCopyWith(
          Translations value, $Res Function(Translations) then) =
      _$TranslationsCopyWithImpl<$Res>;
  $Res call(
      {Kal? ara,
      Kal? ces,
      Kal? cym,
      Kal? deu,
      Kal? est,
      Kal? fin,
      Eng? fra,
      Kal? hrv,
      Kal? hun,
      Kal? ita,
      Kal? jpn,
      Kal? kor,
      Kal? nld,
      Kal? per,
      Kal? pol,
      Kal? por,
      Kal? rus,
      Kal? slk,
      Kal? spa,
      Kal? swe,
      Kal? urd,
      Kal? zho});

  $KalCopyWith<$Res>? get ara;
  $KalCopyWith<$Res>? get ces;
  $KalCopyWith<$Res>? get cym;
  $KalCopyWith<$Res>? get deu;
  $KalCopyWith<$Res>? get est;
  $KalCopyWith<$Res>? get fin;
  $EngCopyWith<$Res>? get fra;
  $KalCopyWith<$Res>? get hrv;
  $KalCopyWith<$Res>? get hun;
  $KalCopyWith<$Res>? get ita;
  $KalCopyWith<$Res>? get jpn;
  $KalCopyWith<$Res>? get kor;
  $KalCopyWith<$Res>? get nld;
  $KalCopyWith<$Res>? get per;
  $KalCopyWith<$Res>? get pol;
  $KalCopyWith<$Res>? get por;
  $KalCopyWith<$Res>? get rus;
  $KalCopyWith<$Res>? get slk;
  $KalCopyWith<$Res>? get spa;
  $KalCopyWith<$Res>? get swe;
  $KalCopyWith<$Res>? get urd;
  $KalCopyWith<$Res>? get zho;
}

/// @nodoc
class _$TranslationsCopyWithImpl<$Res> implements $TranslationsCopyWith<$Res> {
  _$TranslationsCopyWithImpl(this._value, this._then);

  final Translations _value;
  // ignore: unused_field
  final $Res Function(Translations) _then;

  @override
  $Res call({
    Object? ara = freezed,
    Object? ces = freezed,
    Object? cym = freezed,
    Object? deu = freezed,
    Object? est = freezed,
    Object? fin = freezed,
    Object? fra = freezed,
    Object? hrv = freezed,
    Object? hun = freezed,
    Object? ita = freezed,
    Object? jpn = freezed,
    Object? kor = freezed,
    Object? nld = freezed,
    Object? per = freezed,
    Object? pol = freezed,
    Object? por = freezed,
    Object? rus = freezed,
    Object? slk = freezed,
    Object? spa = freezed,
    Object? swe = freezed,
    Object? urd = freezed,
    Object? zho = freezed,
  }) {
    return _then(_value.copyWith(
      ara: ara == freezed
          ? _value.ara
          : ara // ignore: cast_nullable_to_non_nullable
              as Kal?,
      ces: ces == freezed
          ? _value.ces
          : ces // ignore: cast_nullable_to_non_nullable
              as Kal?,
      cym: cym == freezed
          ? _value.cym
          : cym // ignore: cast_nullable_to_non_nullable
              as Kal?,
      deu: deu == freezed
          ? _value.deu
          : deu // ignore: cast_nullable_to_non_nullable
              as Kal?,
      est: est == freezed
          ? _value.est
          : est // ignore: cast_nullable_to_non_nullable
              as Kal?,
      fin: fin == freezed
          ? _value.fin
          : fin // ignore: cast_nullable_to_non_nullable
              as Kal?,
      fra: fra == freezed
          ? _value.fra
          : fra // ignore: cast_nullable_to_non_nullable
              as Eng?,
      hrv: hrv == freezed
          ? _value.hrv
          : hrv // ignore: cast_nullable_to_non_nullable
              as Kal?,
      hun: hun == freezed
          ? _value.hun
          : hun // ignore: cast_nullable_to_non_nullable
              as Kal?,
      ita: ita == freezed
          ? _value.ita
          : ita // ignore: cast_nullable_to_non_nullable
              as Kal?,
      jpn: jpn == freezed
          ? _value.jpn
          : jpn // ignore: cast_nullable_to_non_nullable
              as Kal?,
      kor: kor == freezed
          ? _value.kor
          : kor // ignore: cast_nullable_to_non_nullable
              as Kal?,
      nld: nld == freezed
          ? _value.nld
          : nld // ignore: cast_nullable_to_non_nullable
              as Kal?,
      per: per == freezed
          ? _value.per
          : per // ignore: cast_nullable_to_non_nullable
              as Kal?,
      pol: pol == freezed
          ? _value.pol
          : pol // ignore: cast_nullable_to_non_nullable
              as Kal?,
      por: por == freezed
          ? _value.por
          : por // ignore: cast_nullable_to_non_nullable
              as Kal?,
      rus: rus == freezed
          ? _value.rus
          : rus // ignore: cast_nullable_to_non_nullable
              as Kal?,
      slk: slk == freezed
          ? _value.slk
          : slk // ignore: cast_nullable_to_non_nullable
              as Kal?,
      spa: spa == freezed
          ? _value.spa
          : spa // ignore: cast_nullable_to_non_nullable
              as Kal?,
      swe: swe == freezed
          ? _value.swe
          : swe // ignore: cast_nullable_to_non_nullable
              as Kal?,
      urd: urd == freezed
          ? _value.urd
          : urd // ignore: cast_nullable_to_non_nullable
              as Kal?,
      zho: zho == freezed
          ? _value.zho
          : zho // ignore: cast_nullable_to_non_nullable
              as Kal?,
    ));
  }

  @override
  $KalCopyWith<$Res>? get ara {
    if (_value.ara == null) {
      return null;
    }

    return $KalCopyWith<$Res>(_value.ara!, (value) {
      return _then(_value.copyWith(ara: value));
    });
  }

  @override
  $KalCopyWith<$Res>? get ces {
    if (_value.ces == null) {
      return null;
    }

    return $KalCopyWith<$Res>(_value.ces!, (value) {
      return _then(_value.copyWith(ces: value));
    });
  }

  @override
  $KalCopyWith<$Res>? get cym {
    if (_value.cym == null) {
      return null;
    }

    return $KalCopyWith<$Res>(_value.cym!, (value) {
      return _then(_value.copyWith(cym: value));
    });
  }

  @override
  $KalCopyWith<$Res>? get deu {
    if (_value.deu == null) {
      return null;
    }

    return $KalCopyWith<$Res>(_value.deu!, (value) {
      return _then(_value.copyWith(deu: value));
    });
  }

  @override
  $KalCopyWith<$Res>? get est {
    if (_value.est == null) {
      return null;
    }

    return $KalCopyWith<$Res>(_value.est!, (value) {
      return _then(_value.copyWith(est: value));
    });
  }

  @override
  $KalCopyWith<$Res>? get fin {
    if (_value.fin == null) {
      return null;
    }

    return $KalCopyWith<$Res>(_value.fin!, (value) {
      return _then(_value.copyWith(fin: value));
    });
  }

  @override
  $EngCopyWith<$Res>? get fra {
    if (_value.fra == null) {
      return null;
    }

    return $EngCopyWith<$Res>(_value.fra!, (value) {
      return _then(_value.copyWith(fra: value));
    });
  }

  @override
  $KalCopyWith<$Res>? get hrv {
    if (_value.hrv == null) {
      return null;
    }

    return $KalCopyWith<$Res>(_value.hrv!, (value) {
      return _then(_value.copyWith(hrv: value));
    });
  }

  @override
  $KalCopyWith<$Res>? get hun {
    if (_value.hun == null) {
      return null;
    }

    return $KalCopyWith<$Res>(_value.hun!, (value) {
      return _then(_value.copyWith(hun: value));
    });
  }

  @override
  $KalCopyWith<$Res>? get ita {
    if (_value.ita == null) {
      return null;
    }

    return $KalCopyWith<$Res>(_value.ita!, (value) {
      return _then(_value.copyWith(ita: value));
    });
  }

  @override
  $KalCopyWith<$Res>? get jpn {
    if (_value.jpn == null) {
      return null;
    }

    return $KalCopyWith<$Res>(_value.jpn!, (value) {
      return _then(_value.copyWith(jpn: value));
    });
  }

  @override
  $KalCopyWith<$Res>? get kor {
    if (_value.kor == null) {
      return null;
    }

    return $KalCopyWith<$Res>(_value.kor!, (value) {
      return _then(_value.copyWith(kor: value));
    });
  }

  @override
  $KalCopyWith<$Res>? get nld {
    if (_value.nld == null) {
      return null;
    }

    return $KalCopyWith<$Res>(_value.nld!, (value) {
      return _then(_value.copyWith(nld: value));
    });
  }

  @override
  $KalCopyWith<$Res>? get per {
    if (_value.per == null) {
      return null;
    }

    return $KalCopyWith<$Res>(_value.per!, (value) {
      return _then(_value.copyWith(per: value));
    });
  }

  @override
  $KalCopyWith<$Res>? get pol {
    if (_value.pol == null) {
      return null;
    }

    return $KalCopyWith<$Res>(_value.pol!, (value) {
      return _then(_value.copyWith(pol: value));
    });
  }

  @override
  $KalCopyWith<$Res>? get por {
    if (_value.por == null) {
      return null;
    }

    return $KalCopyWith<$Res>(_value.por!, (value) {
      return _then(_value.copyWith(por: value));
    });
  }

  @override
  $KalCopyWith<$Res>? get rus {
    if (_value.rus == null) {
      return null;
    }

    return $KalCopyWith<$Res>(_value.rus!, (value) {
      return _then(_value.copyWith(rus: value));
    });
  }

  @override
  $KalCopyWith<$Res>? get slk {
    if (_value.slk == null) {
      return null;
    }

    return $KalCopyWith<$Res>(_value.slk!, (value) {
      return _then(_value.copyWith(slk: value));
    });
  }

  @override
  $KalCopyWith<$Res>? get spa {
    if (_value.spa == null) {
      return null;
    }

    return $KalCopyWith<$Res>(_value.spa!, (value) {
      return _then(_value.copyWith(spa: value));
    });
  }

  @override
  $KalCopyWith<$Res>? get swe {
    if (_value.swe == null) {
      return null;
    }

    return $KalCopyWith<$Res>(_value.swe!, (value) {
      return _then(_value.copyWith(swe: value));
    });
  }

  @override
  $KalCopyWith<$Res>? get urd {
    if (_value.urd == null) {
      return null;
    }

    return $KalCopyWith<$Res>(_value.urd!, (value) {
      return _then(_value.copyWith(urd: value));
    });
  }

  @override
  $KalCopyWith<$Res>? get zho {
    if (_value.zho == null) {
      return null;
    }

    return $KalCopyWith<$Res>(_value.zho!, (value) {
      return _then(_value.copyWith(zho: value));
    });
  }
}

/// @nodoc
abstract class _$TranslationsCopyWith<$Res>
    implements $TranslationsCopyWith<$Res> {
  factory _$TranslationsCopyWith(
          _Translations value, $Res Function(_Translations) then) =
      __$TranslationsCopyWithImpl<$Res>;
  @override
  $Res call(
      {Kal? ara,
      Kal? ces,
      Kal? cym,
      Kal? deu,
      Kal? est,
      Kal? fin,
      Eng? fra,
      Kal? hrv,
      Kal? hun,
      Kal? ita,
      Kal? jpn,
      Kal? kor,
      Kal? nld,
      Kal? per,
      Kal? pol,
      Kal? por,
      Kal? rus,
      Kal? slk,
      Kal? spa,
      Kal? swe,
      Kal? urd,
      Kal? zho});

  @override
  $KalCopyWith<$Res>? get ara;
  @override
  $KalCopyWith<$Res>? get ces;
  @override
  $KalCopyWith<$Res>? get cym;
  @override
  $KalCopyWith<$Res>? get deu;
  @override
  $KalCopyWith<$Res>? get est;
  @override
  $KalCopyWith<$Res>? get fin;
  @override
  $EngCopyWith<$Res>? get fra;
  @override
  $KalCopyWith<$Res>? get hrv;
  @override
  $KalCopyWith<$Res>? get hun;
  @override
  $KalCopyWith<$Res>? get ita;
  @override
  $KalCopyWith<$Res>? get jpn;
  @override
  $KalCopyWith<$Res>? get kor;
  @override
  $KalCopyWith<$Res>? get nld;
  @override
  $KalCopyWith<$Res>? get per;
  @override
  $KalCopyWith<$Res>? get pol;
  @override
  $KalCopyWith<$Res>? get por;
  @override
  $KalCopyWith<$Res>? get rus;
  @override
  $KalCopyWith<$Res>? get slk;
  @override
  $KalCopyWith<$Res>? get spa;
  @override
  $KalCopyWith<$Res>? get swe;
  @override
  $KalCopyWith<$Res>? get urd;
  @override
  $KalCopyWith<$Res>? get zho;
}

/// @nodoc
class __$TranslationsCopyWithImpl<$Res> extends _$TranslationsCopyWithImpl<$Res>
    implements _$TranslationsCopyWith<$Res> {
  __$TranslationsCopyWithImpl(
      _Translations _value, $Res Function(_Translations) _then)
      : super(_value, (v) => _then(v as _Translations));

  @override
  _Translations get _value => super._value as _Translations;

  @override
  $Res call({
    Object? ara = freezed,
    Object? ces = freezed,
    Object? cym = freezed,
    Object? deu = freezed,
    Object? est = freezed,
    Object? fin = freezed,
    Object? fra = freezed,
    Object? hrv = freezed,
    Object? hun = freezed,
    Object? ita = freezed,
    Object? jpn = freezed,
    Object? kor = freezed,
    Object? nld = freezed,
    Object? per = freezed,
    Object? pol = freezed,
    Object? por = freezed,
    Object? rus = freezed,
    Object? slk = freezed,
    Object? spa = freezed,
    Object? swe = freezed,
    Object? urd = freezed,
    Object? zho = freezed,
  }) {
    return _then(_Translations(
      ara: ara == freezed
          ? _value.ara
          : ara // ignore: cast_nullable_to_non_nullable
              as Kal?,
      ces: ces == freezed
          ? _value.ces
          : ces // ignore: cast_nullable_to_non_nullable
              as Kal?,
      cym: cym == freezed
          ? _value.cym
          : cym // ignore: cast_nullable_to_non_nullable
              as Kal?,
      deu: deu == freezed
          ? _value.deu
          : deu // ignore: cast_nullable_to_non_nullable
              as Kal?,
      est: est == freezed
          ? _value.est
          : est // ignore: cast_nullable_to_non_nullable
              as Kal?,
      fin: fin == freezed
          ? _value.fin
          : fin // ignore: cast_nullable_to_non_nullable
              as Kal?,
      fra: fra == freezed
          ? _value.fra
          : fra // ignore: cast_nullable_to_non_nullable
              as Eng?,
      hrv: hrv == freezed
          ? _value.hrv
          : hrv // ignore: cast_nullable_to_non_nullable
              as Kal?,
      hun: hun == freezed
          ? _value.hun
          : hun // ignore: cast_nullable_to_non_nullable
              as Kal?,
      ita: ita == freezed
          ? _value.ita
          : ita // ignore: cast_nullable_to_non_nullable
              as Kal?,
      jpn: jpn == freezed
          ? _value.jpn
          : jpn // ignore: cast_nullable_to_non_nullable
              as Kal?,
      kor: kor == freezed
          ? _value.kor
          : kor // ignore: cast_nullable_to_non_nullable
              as Kal?,
      nld: nld == freezed
          ? _value.nld
          : nld // ignore: cast_nullable_to_non_nullable
              as Kal?,
      per: per == freezed
          ? _value.per
          : per // ignore: cast_nullable_to_non_nullable
              as Kal?,
      pol: pol == freezed
          ? _value.pol
          : pol // ignore: cast_nullable_to_non_nullable
              as Kal?,
      por: por == freezed
          ? _value.por
          : por // ignore: cast_nullable_to_non_nullable
              as Kal?,
      rus: rus == freezed
          ? _value.rus
          : rus // ignore: cast_nullable_to_non_nullable
              as Kal?,
      slk: slk == freezed
          ? _value.slk
          : slk // ignore: cast_nullable_to_non_nullable
              as Kal?,
      spa: spa == freezed
          ? _value.spa
          : spa // ignore: cast_nullable_to_non_nullable
              as Kal?,
      swe: swe == freezed
          ? _value.swe
          : swe // ignore: cast_nullable_to_non_nullable
              as Kal?,
      urd: urd == freezed
          ? _value.urd
          : urd // ignore: cast_nullable_to_non_nullable
              as Kal?,
      zho: zho == freezed
          ? _value.zho
          : zho // ignore: cast_nullable_to_non_nullable
              as Kal?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Translations extends _Translations {
  const _$_Translations(
      {required this.ara,
      required this.ces,
      required this.cym,
      required this.deu,
      required this.est,
      required this.fin,
      required this.fra,
      required this.hrv,
      required this.hun,
      required this.ita,
      required this.jpn,
      required this.kor,
      required this.nld,
      required this.per,
      required this.pol,
      required this.por,
      required this.rus,
      required this.slk,
      required this.spa,
      required this.swe,
      required this.urd,
      required this.zho})
      : super._();

  factory _$_Translations.fromJson(Map<String, dynamic> json) =>
      _$$_TranslationsFromJson(json);

  @override
  final Kal? ara;
  @override
  final Kal? ces;
  @override
  final Kal? cym;
  @override
  final Kal? deu;
  @override
  final Kal? est;
  @override
  final Kal? fin;
  @override
  final Eng? fra;
  @override
  final Kal? hrv;
  @override
  final Kal? hun;
  @override
  final Kal? ita;
  @override
  final Kal? jpn;
  @override
  final Kal? kor;
  @override
  final Kal? nld;
  @override
  final Kal? per;
  @override
  final Kal? pol;
  @override
  final Kal? por;
  @override
  final Kal? rus;
  @override
  final Kal? slk;
  @override
  final Kal? spa;
  @override
  final Kal? swe;
  @override
  final Kal? urd;
  @override
  final Kal? zho;

  @override
  String toString() {
    return 'Translations(ara: $ara, ces: $ces, cym: $cym, deu: $deu, est: $est, fin: $fin, fra: $fra, hrv: $hrv, hun: $hun, ita: $ita, jpn: $jpn, kor: $kor, nld: $nld, per: $per, pol: $pol, por: $por, rus: $rus, slk: $slk, spa: $spa, swe: $swe, urd: $urd, zho: $zho)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Translations &&
            const DeepCollectionEquality().equals(other.ara, ara) &&
            const DeepCollectionEquality().equals(other.ces, ces) &&
            const DeepCollectionEquality().equals(other.cym, cym) &&
            const DeepCollectionEquality().equals(other.deu, deu) &&
            const DeepCollectionEquality().equals(other.est, est) &&
            const DeepCollectionEquality().equals(other.fin, fin) &&
            const DeepCollectionEquality().equals(other.fra, fra) &&
            const DeepCollectionEquality().equals(other.hrv, hrv) &&
            const DeepCollectionEquality().equals(other.hun, hun) &&
            const DeepCollectionEquality().equals(other.ita, ita) &&
            const DeepCollectionEquality().equals(other.jpn, jpn) &&
            const DeepCollectionEquality().equals(other.kor, kor) &&
            const DeepCollectionEquality().equals(other.nld, nld) &&
            const DeepCollectionEquality().equals(other.per, per) &&
            const DeepCollectionEquality().equals(other.pol, pol) &&
            const DeepCollectionEquality().equals(other.por, por) &&
            const DeepCollectionEquality().equals(other.rus, rus) &&
            const DeepCollectionEquality().equals(other.slk, slk) &&
            const DeepCollectionEquality().equals(other.spa, spa) &&
            const DeepCollectionEquality().equals(other.swe, swe) &&
            const DeepCollectionEquality().equals(other.urd, urd) &&
            const DeepCollectionEquality().equals(other.zho, zho));
  }

  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        const DeepCollectionEquality().hash(ara),
        const DeepCollectionEquality().hash(ces),
        const DeepCollectionEquality().hash(cym),
        const DeepCollectionEquality().hash(deu),
        const DeepCollectionEquality().hash(est),
        const DeepCollectionEquality().hash(fin),
        const DeepCollectionEquality().hash(fra),
        const DeepCollectionEquality().hash(hrv),
        const DeepCollectionEquality().hash(hun),
        const DeepCollectionEquality().hash(ita),
        const DeepCollectionEquality().hash(jpn),
        const DeepCollectionEquality().hash(kor),
        const DeepCollectionEquality().hash(nld),
        const DeepCollectionEquality().hash(per),
        const DeepCollectionEquality().hash(pol),
        const DeepCollectionEquality().hash(por),
        const DeepCollectionEquality().hash(rus),
        const DeepCollectionEquality().hash(slk),
        const DeepCollectionEquality().hash(spa),
        const DeepCollectionEquality().hash(swe),
        const DeepCollectionEquality().hash(urd),
        const DeepCollectionEquality().hash(zho)
      ]);

  @JsonKey(ignore: true)
  @override
  _$TranslationsCopyWith<_Translations> get copyWith =>
      __$TranslationsCopyWithImpl<_Translations>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TranslationsToJson(this);
  }
}

abstract class _Translations extends Translations {
  const factory _Translations(
      {required Kal? ara,
      required Kal? ces,
      required Kal? cym,
      required Kal? deu,
      required Kal? est,
      required Kal? fin,
      required Eng? fra,
      required Kal? hrv,
      required Kal? hun,
      required Kal? ita,
      required Kal? jpn,
      required Kal? kor,
      required Kal? nld,
      required Kal? per,
      required Kal? pol,
      required Kal? por,
      required Kal? rus,
      required Kal? slk,
      required Kal? spa,
      required Kal? swe,
      required Kal? urd,
      required Kal? zho}) = _$_Translations;
  const _Translations._() : super._();

  factory _Translations.fromJson(Map<String, dynamic> json) =
      _$_Translations.fromJson;

  @override
  Kal? get ara;
  @override
  Kal? get ces;
  @override
  Kal? get cym;
  @override
  Kal? get deu;
  @override
  Kal? get est;
  @override
  Kal? get fin;
  @override
  Eng? get fra;
  @override
  Kal? get hrv;
  @override
  Kal? get hun;
  @override
  Kal? get ita;
  @override
  Kal? get jpn;
  @override
  Kal? get kor;
  @override
  Kal? get nld;
  @override
  Kal? get per;
  @override
  Kal? get pol;
  @override
  Kal? get por;
  @override
  Kal? get rus;
  @override
  Kal? get slk;
  @override
  Kal? get spa;
  @override
  Kal? get swe;
  @override
  Kal? get urd;
  @override
  Kal? get zho;
  @override
  @JsonKey(ignore: true)
  _$TranslationsCopyWith<_Translations> get copyWith =>
      throw _privateConstructorUsedError;
}
