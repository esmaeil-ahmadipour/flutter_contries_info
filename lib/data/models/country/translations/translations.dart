import 'package:flutter_countries_info/data/models/country/eng/eng.dart';
import 'package:flutter_countries_info/data/models/country/kal/kal.dart';
import 'package:freezed_annotation/freezed_annotation.dart' ;
part 'translations.g.dart';
part 'translations.freezed.dart';

@freezed
class Translations with _$Translations {
  const Translations._();
  const factory Translations({
    required    Kal? ara,
    required    Kal? ces,
    required   Kal? cym,
    required Kal? deu,
    required   Kal? est,
    required  Kal? fin,
    required  Eng? fra,
    required   Kal? hrv,
    required  Kal? hun,
    required  Kal? ita,
    required  Kal? jpn,
    required  Kal? kor,
    required Kal? nld,
    required  Kal? per,
    required  Kal? pol,
    required   Kal? por,
    required   Kal? rus,
    required   Kal? slk,
    required   Kal? spa,
    required   Kal? swe,
    required   Kal? urd,
    required    Kal? zho,
  }) = _Translations;
  factory Translations.fromJson(Map<String, dynamic> json) => _$TranslationsFromJson(json);
}
