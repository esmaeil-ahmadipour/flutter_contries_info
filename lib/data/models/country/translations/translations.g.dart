// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'translations.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Translations _$$_TranslationsFromJson(Map<String, dynamic> json) =>
    _$_Translations(
      ara: json['ara'] == null
          ? null
          : Kal.fromJson(json['ara'] as Map<String, dynamic>),
      ces: json['ces'] == null
          ? null
          : Kal.fromJson(json['ces'] as Map<String, dynamic>),
      cym: json['cym'] == null
          ? null
          : Kal.fromJson(json['cym'] as Map<String, dynamic>),
      deu: json['deu'] == null
          ? null
          : Kal.fromJson(json['deu'] as Map<String, dynamic>),
      est: json['est'] == null
          ? null
          : Kal.fromJson(json['est'] as Map<String, dynamic>),
      fin: json['fin'] == null
          ? null
          : Kal.fromJson(json['fin'] as Map<String, dynamic>),
      fra: json['fra'] == null
          ? null
          : Eng.fromJson(json['fra'] as Map<String, dynamic>),
      hrv: json['hrv'] == null
          ? null
          : Kal.fromJson(json['hrv'] as Map<String, dynamic>),
      hun: json['hun'] == null
          ? null
          : Kal.fromJson(json['hun'] as Map<String, dynamic>),
      ita: json['ita'] == null
          ? null
          : Kal.fromJson(json['ita'] as Map<String, dynamic>),
      jpn: json['jpn'] == null
          ? null
          : Kal.fromJson(json['jpn'] as Map<String, dynamic>),
      kor: json['kor'] == null
          ? null
          : Kal.fromJson(json['kor'] as Map<String, dynamic>),
      nld: json['nld'] == null
          ? null
          : Kal.fromJson(json['nld'] as Map<String, dynamic>),
      per: json['per'] == null
          ? null
          : Kal.fromJson(json['per'] as Map<String, dynamic>),
      pol: json['pol'] == null
          ? null
          : Kal.fromJson(json['pol'] as Map<String, dynamic>),
      por: json['por'] == null
          ? null
          : Kal.fromJson(json['por'] as Map<String, dynamic>),
      rus: json['rus'] == null
          ? null
          : Kal.fromJson(json['rus'] as Map<String, dynamic>),
      slk: json['slk'] == null
          ? null
          : Kal.fromJson(json['slk'] as Map<String, dynamic>),
      spa: json['spa'] == null
          ? null
          : Kal.fromJson(json['spa'] as Map<String, dynamic>),
      swe: json['swe'] == null
          ? null
          : Kal.fromJson(json['swe'] as Map<String, dynamic>),
      urd: json['urd'] == null
          ? null
          : Kal.fromJson(json['urd'] as Map<String, dynamic>),
      zho: json['zho'] == null
          ? null
          : Kal.fromJson(json['zho'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_TranslationsToJson(_$_Translations instance) =>
    <String, dynamic>{
      'ara': instance.ara,
      'ces': instance.ces,
      'cym': instance.cym,
      'deu': instance.deu,
      'est': instance.est,
      'fin': instance.fin,
      'fra': instance.fra,
      'hrv': instance.hrv,
      'hun': instance.hun,
      'ita': instance.ita,
      'jpn': instance.jpn,
      'kor': instance.kor,
      'nld': instance.nld,
      'per': instance.per,
      'pol': instance.pol,
      'por': instance.por,
      'rus': instance.rus,
      'slk': instance.slk,
      'spa': instance.spa,
      'swe': instance.swe,
      'urd': instance.urd,
      'zho': instance.zho,
    };
