import 'package:freezed_annotation/freezed_annotation.dart' ;
part 'car.g.dart';
part 'car.freezed.dart';

@freezed
class Car with _$Car {
  const Car._();
  const factory Car({
   required List<String>? signs,
   required String? side,

  }) = _Car;
  factory Car.fromJson(Map<String, dynamic> json) => _$CarFromJson(json);
}
