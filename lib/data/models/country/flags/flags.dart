import 'package:freezed_annotation/freezed_annotation.dart' ;
part 'flags.g.dart';
part 'flags.freezed.dart';

@freezed
class Flags with _$Flags {
  const Flags._();
  const factory Flags({
    required  String? png,
    required  String? svg,
  }) = _Flags;
  factory Flags.fromJson(Map<String, dynamic> json) => _$FlagsFromJson(json);
}
