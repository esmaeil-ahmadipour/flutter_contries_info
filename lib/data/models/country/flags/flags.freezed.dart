// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'flags.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Flags _$FlagsFromJson(Map<String, dynamic> json) {
  return _Flags.fromJson(json);
}

/// @nodoc
class _$FlagsTearOff {
  const _$FlagsTearOff();

  _Flags call({required String? png, required String? svg}) {
    return _Flags(
      png: png,
      svg: svg,
    );
  }

  Flags fromJson(Map<String, Object?> json) {
    return Flags.fromJson(json);
  }
}

/// @nodoc
const $Flags = _$FlagsTearOff();

/// @nodoc
mixin _$Flags {
  String? get png => throw _privateConstructorUsedError;
  String? get svg => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FlagsCopyWith<Flags> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FlagsCopyWith<$Res> {
  factory $FlagsCopyWith(Flags value, $Res Function(Flags) then) =
      _$FlagsCopyWithImpl<$Res>;
  $Res call({String? png, String? svg});
}

/// @nodoc
class _$FlagsCopyWithImpl<$Res> implements $FlagsCopyWith<$Res> {
  _$FlagsCopyWithImpl(this._value, this._then);

  final Flags _value;
  // ignore: unused_field
  final $Res Function(Flags) _then;

  @override
  $Res call({
    Object? png = freezed,
    Object? svg = freezed,
  }) {
    return _then(_value.copyWith(
      png: png == freezed
          ? _value.png
          : png // ignore: cast_nullable_to_non_nullable
              as String?,
      svg: svg == freezed
          ? _value.svg
          : svg // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$FlagsCopyWith<$Res> implements $FlagsCopyWith<$Res> {
  factory _$FlagsCopyWith(_Flags value, $Res Function(_Flags) then) =
      __$FlagsCopyWithImpl<$Res>;
  @override
  $Res call({String? png, String? svg});
}

/// @nodoc
class __$FlagsCopyWithImpl<$Res> extends _$FlagsCopyWithImpl<$Res>
    implements _$FlagsCopyWith<$Res> {
  __$FlagsCopyWithImpl(_Flags _value, $Res Function(_Flags) _then)
      : super(_value, (v) => _then(v as _Flags));

  @override
  _Flags get _value => super._value as _Flags;

  @override
  $Res call({
    Object? png = freezed,
    Object? svg = freezed,
  }) {
    return _then(_Flags(
      png: png == freezed
          ? _value.png
          : png // ignore: cast_nullable_to_non_nullable
              as String?,
      svg: svg == freezed
          ? _value.svg
          : svg // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Flags extends _Flags {
  const _$_Flags({required this.png, required this.svg}) : super._();

  factory _$_Flags.fromJson(Map<String, dynamic> json) =>
      _$$_FlagsFromJson(json);

  @override
  final String? png;
  @override
  final String? svg;

  @override
  String toString() {
    return 'Flags(png: $png, svg: $svg)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Flags &&
            const DeepCollectionEquality().equals(other.png, png) &&
            const DeepCollectionEquality().equals(other.svg, svg));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(png),
      const DeepCollectionEquality().hash(svg));

  @JsonKey(ignore: true)
  @override
  _$FlagsCopyWith<_Flags> get copyWith =>
      __$FlagsCopyWithImpl<_Flags>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_FlagsToJson(this);
  }
}

abstract class _Flags extends Flags {
  const factory _Flags({required String? png, required String? svg}) = _$_Flags;
  const _Flags._() : super._();

  factory _Flags.fromJson(Map<String, dynamic> json) = _$_Flags.fromJson;

  @override
  String? get png;
  @override
  String? get svg;
  @override
  @JsonKey(ignore: true)
  _$FlagsCopyWith<_Flags> get copyWith => throw _privateConstructorUsedError;
}
