import 'package:freezed_annotation/freezed_annotation.dart' ;
part 'postalCode.g.dart';
part 'postalCode.freezed.dart';

@freezed
class PostalCode with _$PostalCode {
  const PostalCode._();
  const factory PostalCode({
    required String? format,
    required String? regex,
  }) = _PostalCode;
  factory PostalCode.fromJson(Map<String, dynamic> json) => _$PostalCodeFromJson(json);
}
