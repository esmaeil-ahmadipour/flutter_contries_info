// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'postalCode.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PostalCode _$PostalCodeFromJson(Map<String, dynamic> json) {
  return _PostalCode.fromJson(json);
}

/// @nodoc
class _$PostalCodeTearOff {
  const _$PostalCodeTearOff();

  _PostalCode call({required String? format, required String? regex}) {
    return _PostalCode(
      format: format,
      regex: regex,
    );
  }

  PostalCode fromJson(Map<String, Object?> json) {
    return PostalCode.fromJson(json);
  }
}

/// @nodoc
const $PostalCode = _$PostalCodeTearOff();

/// @nodoc
mixin _$PostalCode {
  String? get format => throw _privateConstructorUsedError;
  String? get regex => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PostalCodeCopyWith<PostalCode> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PostalCodeCopyWith<$Res> {
  factory $PostalCodeCopyWith(
          PostalCode value, $Res Function(PostalCode) then) =
      _$PostalCodeCopyWithImpl<$Res>;
  $Res call({String? format, String? regex});
}

/// @nodoc
class _$PostalCodeCopyWithImpl<$Res> implements $PostalCodeCopyWith<$Res> {
  _$PostalCodeCopyWithImpl(this._value, this._then);

  final PostalCode _value;
  // ignore: unused_field
  final $Res Function(PostalCode) _then;

  @override
  $Res call({
    Object? format = freezed,
    Object? regex = freezed,
  }) {
    return _then(_value.copyWith(
      format: format == freezed
          ? _value.format
          : format // ignore: cast_nullable_to_non_nullable
              as String?,
      regex: regex == freezed
          ? _value.regex
          : regex // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$PostalCodeCopyWith<$Res> implements $PostalCodeCopyWith<$Res> {
  factory _$PostalCodeCopyWith(
          _PostalCode value, $Res Function(_PostalCode) then) =
      __$PostalCodeCopyWithImpl<$Res>;
  @override
  $Res call({String? format, String? regex});
}

/// @nodoc
class __$PostalCodeCopyWithImpl<$Res> extends _$PostalCodeCopyWithImpl<$Res>
    implements _$PostalCodeCopyWith<$Res> {
  __$PostalCodeCopyWithImpl(
      _PostalCode _value, $Res Function(_PostalCode) _then)
      : super(_value, (v) => _then(v as _PostalCode));

  @override
  _PostalCode get _value => super._value as _PostalCode;

  @override
  $Res call({
    Object? format = freezed,
    Object? regex = freezed,
  }) {
    return _then(_PostalCode(
      format: format == freezed
          ? _value.format
          : format // ignore: cast_nullable_to_non_nullable
              as String?,
      regex: regex == freezed
          ? _value.regex
          : regex // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_PostalCode extends _PostalCode {
  const _$_PostalCode({required this.format, required this.regex}) : super._();

  factory _$_PostalCode.fromJson(Map<String, dynamic> json) =>
      _$$_PostalCodeFromJson(json);

  @override
  final String? format;
  @override
  final String? regex;

  @override
  String toString() {
    return 'PostalCode(format: $format, regex: $regex)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _PostalCode &&
            const DeepCollectionEquality().equals(other.format, format) &&
            const DeepCollectionEquality().equals(other.regex, regex));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(format),
      const DeepCollectionEquality().hash(regex));

  @JsonKey(ignore: true)
  @override
  _$PostalCodeCopyWith<_PostalCode> get copyWith =>
      __$PostalCodeCopyWithImpl<_PostalCode>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PostalCodeToJson(this);
  }
}

abstract class _PostalCode extends PostalCode {
  const factory _PostalCode({required String? format, required String? regex}) =
      _$_PostalCode;
  const _PostalCode._() : super._();

  factory _PostalCode.fromJson(Map<String, dynamic> json) =
      _$_PostalCode.fromJson;

  @override
  String? get format;
  @override
  String? get regex;
  @override
  @JsonKey(ignore: true)
  _$PostalCodeCopyWith<_PostalCode> get copyWith =>
      throw _privateConstructorUsedError;
}
