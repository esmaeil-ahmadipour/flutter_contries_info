// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'capitalInfo.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CapitalInfo _$CapitalInfoFromJson(Map<String, dynamic> json) {
  return _CapitalInfo.fromJson(json);
}

/// @nodoc
class _$CapitalInfoTearOff {
  const _$CapitalInfoTearOff();

  _CapitalInfo call({required List<double>? latlng}) {
    return _CapitalInfo(
      latlng: latlng,
    );
  }

  CapitalInfo fromJson(Map<String, Object?> json) {
    return CapitalInfo.fromJson(json);
  }
}

/// @nodoc
const $CapitalInfo = _$CapitalInfoTearOff();

/// @nodoc
mixin _$CapitalInfo {
  List<double>? get latlng => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CapitalInfoCopyWith<CapitalInfo> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CapitalInfoCopyWith<$Res> {
  factory $CapitalInfoCopyWith(
          CapitalInfo value, $Res Function(CapitalInfo) then) =
      _$CapitalInfoCopyWithImpl<$Res>;
  $Res call({List<double>? latlng});
}

/// @nodoc
class _$CapitalInfoCopyWithImpl<$Res> implements $CapitalInfoCopyWith<$Res> {
  _$CapitalInfoCopyWithImpl(this._value, this._then);

  final CapitalInfo _value;
  // ignore: unused_field
  final $Res Function(CapitalInfo) _then;

  @override
  $Res call({
    Object? latlng = freezed,
  }) {
    return _then(_value.copyWith(
      latlng: latlng == freezed
          ? _value.latlng
          : latlng // ignore: cast_nullable_to_non_nullable
              as List<double>?,
    ));
  }
}

/// @nodoc
abstract class _$CapitalInfoCopyWith<$Res>
    implements $CapitalInfoCopyWith<$Res> {
  factory _$CapitalInfoCopyWith(
          _CapitalInfo value, $Res Function(_CapitalInfo) then) =
      __$CapitalInfoCopyWithImpl<$Res>;
  @override
  $Res call({List<double>? latlng});
}

/// @nodoc
class __$CapitalInfoCopyWithImpl<$Res> extends _$CapitalInfoCopyWithImpl<$Res>
    implements _$CapitalInfoCopyWith<$Res> {
  __$CapitalInfoCopyWithImpl(
      _CapitalInfo _value, $Res Function(_CapitalInfo) _then)
      : super(_value, (v) => _then(v as _CapitalInfo));

  @override
  _CapitalInfo get _value => super._value as _CapitalInfo;

  @override
  $Res call({
    Object? latlng = freezed,
  }) {
    return _then(_CapitalInfo(
      latlng: latlng == freezed
          ? _value.latlng
          : latlng // ignore: cast_nullable_to_non_nullable
              as List<double>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CapitalInfo extends _CapitalInfo {
  const _$_CapitalInfo({required this.latlng}) : super._();

  factory _$_CapitalInfo.fromJson(Map<String, dynamic> json) =>
      _$$_CapitalInfoFromJson(json);

  @override
  final List<double>? latlng;

  @override
  String toString() {
    return 'CapitalInfo(latlng: $latlng)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _CapitalInfo &&
            const DeepCollectionEquality().equals(other.latlng, latlng));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(latlng));

  @JsonKey(ignore: true)
  @override
  _$CapitalInfoCopyWith<_CapitalInfo> get copyWith =>
      __$CapitalInfoCopyWithImpl<_CapitalInfo>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CapitalInfoToJson(this);
  }
}

abstract class _CapitalInfo extends CapitalInfo {
  const factory _CapitalInfo({required List<double>? latlng}) = _$_CapitalInfo;
  const _CapitalInfo._() : super._();

  factory _CapitalInfo.fromJson(Map<String, dynamic> json) =
      _$_CapitalInfo.fromJson;

  @override
  List<double>? get latlng;
  @override
  @JsonKey(ignore: true)
  _$CapitalInfoCopyWith<_CapitalInfo> get copyWith =>
      throw _privateConstructorUsedError;
}
