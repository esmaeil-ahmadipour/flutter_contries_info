import 'package:freezed_annotation/freezed_annotation.dart' ;
part 'capitalInfo.g.dart';
part 'capitalInfo.freezed.dart';

@freezed
class CapitalInfo with _$CapitalInfo {
  const CapitalInfo._();
  const factory CapitalInfo({
    required   List<double>? latlng,
  }) = _CapitalInfo;
  factory CapitalInfo.fromJson(Map<String, dynamic> json) => _$CapitalInfoFromJson(json);
}
