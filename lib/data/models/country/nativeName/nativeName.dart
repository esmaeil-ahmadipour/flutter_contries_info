import 'package:flutter_countries_info/data/models/country/kal/kal.dart';
import 'package:freezed_annotation/freezed_annotation.dart' ;
part 'nativeName.g.dart';
part 'nativeName.freezed.dart';

@freezed
class NativeName with _$NativeName {
  const NativeName._();
  const factory NativeName({
    required Kal? kal,

  }) = _NativeName;
  factory NativeName.fromJson(Map<String, dynamic> json) => _$NativeNameFromJson(json);
}
