// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'nativeName.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

NativeName _$NativeNameFromJson(Map<String, dynamic> json) {
  return _NativeName.fromJson(json);
}

/// @nodoc
class _$NativeNameTearOff {
  const _$NativeNameTearOff();

  _NativeName call({required Kal? kal}) {
    return _NativeName(
      kal: kal,
    );
  }

  NativeName fromJson(Map<String, Object?> json) {
    return NativeName.fromJson(json);
  }
}

/// @nodoc
const $NativeName = _$NativeNameTearOff();

/// @nodoc
mixin _$NativeName {
  Kal? get kal => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $NativeNameCopyWith<NativeName> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NativeNameCopyWith<$Res> {
  factory $NativeNameCopyWith(
          NativeName value, $Res Function(NativeName) then) =
      _$NativeNameCopyWithImpl<$Res>;
  $Res call({Kal? kal});

  $KalCopyWith<$Res>? get kal;
}

/// @nodoc
class _$NativeNameCopyWithImpl<$Res> implements $NativeNameCopyWith<$Res> {
  _$NativeNameCopyWithImpl(this._value, this._then);

  final NativeName _value;
  // ignore: unused_field
  final $Res Function(NativeName) _then;

  @override
  $Res call({
    Object? kal = freezed,
  }) {
    return _then(_value.copyWith(
      kal: kal == freezed
          ? _value.kal
          : kal // ignore: cast_nullable_to_non_nullable
              as Kal?,
    ));
  }

  @override
  $KalCopyWith<$Res>? get kal {
    if (_value.kal == null) {
      return null;
    }

    return $KalCopyWith<$Res>(_value.kal!, (value) {
      return _then(_value.copyWith(kal: value));
    });
  }
}

/// @nodoc
abstract class _$NativeNameCopyWith<$Res> implements $NativeNameCopyWith<$Res> {
  factory _$NativeNameCopyWith(
          _NativeName value, $Res Function(_NativeName) then) =
      __$NativeNameCopyWithImpl<$Res>;
  @override
  $Res call({Kal? kal});

  @override
  $KalCopyWith<$Res>? get kal;
}

/// @nodoc
class __$NativeNameCopyWithImpl<$Res> extends _$NativeNameCopyWithImpl<$Res>
    implements _$NativeNameCopyWith<$Res> {
  __$NativeNameCopyWithImpl(
      _NativeName _value, $Res Function(_NativeName) _then)
      : super(_value, (v) => _then(v as _NativeName));

  @override
  _NativeName get _value => super._value as _NativeName;

  @override
  $Res call({
    Object? kal = freezed,
  }) {
    return _then(_NativeName(
      kal: kal == freezed
          ? _value.kal
          : kal // ignore: cast_nullable_to_non_nullable
              as Kal?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_NativeName extends _NativeName {
  const _$_NativeName({required this.kal}) : super._();

  factory _$_NativeName.fromJson(Map<String, dynamic> json) =>
      _$$_NativeNameFromJson(json);

  @override
  final Kal? kal;

  @override
  String toString() {
    return 'NativeName(kal: $kal)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _NativeName &&
            const DeepCollectionEquality().equals(other.kal, kal));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(kal));

  @JsonKey(ignore: true)
  @override
  _$NativeNameCopyWith<_NativeName> get copyWith =>
      __$NativeNameCopyWithImpl<_NativeName>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_NativeNameToJson(this);
  }
}

abstract class _NativeName extends NativeName {
  const factory _NativeName({required Kal? kal}) = _$_NativeName;
  const _NativeName._() : super._();

  factory _NativeName.fromJson(Map<String, dynamic> json) =
      _$_NativeName.fromJson;

  @override
  Kal? get kal;
  @override
  @JsonKey(ignore: true)
  _$NativeNameCopyWith<_NativeName> get copyWith =>
      throw _privateConstructorUsedError;
}
