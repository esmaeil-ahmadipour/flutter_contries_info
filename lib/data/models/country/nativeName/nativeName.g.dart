// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'nativeName.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_NativeName _$$_NativeNameFromJson(Map<String, dynamic> json) =>
    _$_NativeName(
      kal: json['kal'] == null
          ? null
          : Kal.fromJson(json['kal'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_NativeNameToJson(_$_NativeName instance) =>
    <String, dynamic>{
      'kal': instance.kal,
    };
