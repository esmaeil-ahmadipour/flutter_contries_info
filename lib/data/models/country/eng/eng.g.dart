// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'eng.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Eng _$$_EngFromJson(Map<String, dynamic> json) => _$_Eng(
      f: json['f'] as String?,
      m: json['m'] as String?,
    );

Map<String, dynamic> _$$_EngToJson(_$_Eng instance) => <String, dynamic>{
      'f': instance.f,
      'm': instance.m,
    };
