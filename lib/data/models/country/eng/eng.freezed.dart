// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'eng.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Eng _$EngFromJson(Map<String, dynamic> json) {
  return _Eng.fromJson(json);
}

/// @nodoc
class _$EngTearOff {
  const _$EngTearOff();

  _Eng call({required String? f, required String? m}) {
    return _Eng(
      f: f,
      m: m,
    );
  }

  Eng fromJson(Map<String, Object?> json) {
    return Eng.fromJson(json);
  }
}

/// @nodoc
const $Eng = _$EngTearOff();

/// @nodoc
mixin _$Eng {
  String? get f => throw _privateConstructorUsedError;
  String? get m => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $EngCopyWith<Eng> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EngCopyWith<$Res> {
  factory $EngCopyWith(Eng value, $Res Function(Eng) then) =
      _$EngCopyWithImpl<$Res>;
  $Res call({String? f, String? m});
}

/// @nodoc
class _$EngCopyWithImpl<$Res> implements $EngCopyWith<$Res> {
  _$EngCopyWithImpl(this._value, this._then);

  final Eng _value;
  // ignore: unused_field
  final $Res Function(Eng) _then;

  @override
  $Res call({
    Object? f = freezed,
    Object? m = freezed,
  }) {
    return _then(_value.copyWith(
      f: f == freezed
          ? _value.f
          : f // ignore: cast_nullable_to_non_nullable
              as String?,
      m: m == freezed
          ? _value.m
          : m // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$EngCopyWith<$Res> implements $EngCopyWith<$Res> {
  factory _$EngCopyWith(_Eng value, $Res Function(_Eng) then) =
      __$EngCopyWithImpl<$Res>;
  @override
  $Res call({String? f, String? m});
}

/// @nodoc
class __$EngCopyWithImpl<$Res> extends _$EngCopyWithImpl<$Res>
    implements _$EngCopyWith<$Res> {
  __$EngCopyWithImpl(_Eng _value, $Res Function(_Eng) _then)
      : super(_value, (v) => _then(v as _Eng));

  @override
  _Eng get _value => super._value as _Eng;

  @override
  $Res call({
    Object? f = freezed,
    Object? m = freezed,
  }) {
    return _then(_Eng(
      f: f == freezed
          ? _value.f
          : f // ignore: cast_nullable_to_non_nullable
              as String?,
      m: m == freezed
          ? _value.m
          : m // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Eng extends _Eng {
  const _$_Eng({required this.f, required this.m}) : super._();

  factory _$_Eng.fromJson(Map<String, dynamic> json) => _$$_EngFromJson(json);

  @override
  final String? f;
  @override
  final String? m;

  @override
  String toString() {
    return 'Eng(f: $f, m: $m)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Eng &&
            const DeepCollectionEquality().equals(other.f, f) &&
            const DeepCollectionEquality().equals(other.m, m));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(f),
      const DeepCollectionEquality().hash(m));

  @JsonKey(ignore: true)
  @override
  _$EngCopyWith<_Eng> get copyWith =>
      __$EngCopyWithImpl<_Eng>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_EngToJson(this);
  }
}

abstract class _Eng extends Eng {
  const factory _Eng({required String? f, required String? m}) = _$_Eng;
  const _Eng._() : super._();

  factory _Eng.fromJson(Map<String, dynamic> json) = _$_Eng.fromJson;

  @override
  String? get f;
  @override
  String? get m;
  @override
  @JsonKey(ignore: true)
  _$EngCopyWith<_Eng> get copyWith => throw _privateConstructorUsedError;
}
