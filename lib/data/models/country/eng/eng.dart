import 'package:freezed_annotation/freezed_annotation.dart' ;
part 'eng.g.dart';
part 'eng.freezed.dart';

@freezed
class Eng with _$Eng {
  const Eng._();
  const factory Eng({
    required String? f,
    required String? m,

  }) = _Eng;
  factory Eng.fromJson(Map<String, dynamic> json) => _$EngFromJson(json);
}
