import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter_countries_info/core/params/countriesInfoParams.dart';
import 'package:flutter_countries_info/data/data_sources/remote/remoteDataState.dart';
import 'package:flutter_countries_info/data/datasources/remote/countriesInfoApiService.dart';
import 'package:flutter_countries_info/data/models/country/country.dart';
import 'package:flutter_countries_info/domain/repositories/countriesInfoRepository.dart';

class CountriesInfoRepositoryImpl implements CountriesInfoRepository {
  final CountriesInfoApiService _countriesInfoApiService;

  const CountriesInfoRepositoryImpl(this._countriesInfoApiService);


  @override
  Future<RemoteDataState<List<Country>>> getCountriesInfo() async{
    try {
      final httpResponse = await _countriesInfoApiService.getAllCountries(
      );
      if (httpResponse.response.statusCode == HttpStatus.ok) {
        print("ss###:${httpResponse.response.data}");

        return RemoteDataSuccess(remoteData: httpResponse.data);
      }else{
        print("nn###:${HttpStatus.ok}");

      }
      return RemoteDataFailed(remoteData: httpResponse.data,
        error: DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          type: DioErrorType.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      print("eee###:${e}");

      return RemoteDataFailed(error: e);
    }
  }


}
